\section{Physical model}
\label{model}
\paragraph{Problem} To determine if the current speed of the downhill biker is too high for the next curve.
For that purpose a model, which is able to describe the physical circumstances, is necessary.

\paragraph{Goal} To define a reasonable physical model and identify the corresponding parameters and forces.

\subsection{Approach}
The chosen model is an approximation of the physical circumstance
and should model the environment in an appropriate manner.
To reduce the complexity of the model and avoid the need of external sensors, the physics of the bicycle like the slope of the steering wheel, weather circumstances, air resistance and more were ignored.

The parameters focused on are listed below. They are given by the defined tracks, added as a setting or are provided by the built-in sensors of the smartphone.
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[m:] Driver weight [\SI{}{\kilogram}]
 \item[v:] Velocity [\SI[per-mode=fraction]{}{\metre\per\second}]
 \item[$\mu$:] Friction coefficient
 \item[r:] Curve radius [\SI{}{\metre}]
 \item[$\alpha$:] Slope angle [$\degree$]
 \item[g:] Gravity acceleration [\SI[per-mode=fraction]{}{\metre\per\second\squared}]
\end{itemize}

Firgure \ref{fig:physic} shows a bicycle in a banked curve and the relevant forces.
The goal of the system is to let force $\vec{F_{A}}$ always be smaller or equal to the sum of the forces $\vec{F_{B}}$ and $\vec{F_{R}}$.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.65\textwidth,height=0.65\textheight,keepaspectratio]{images/physic_model.png}
    \caption{Physical model}
    \label{fig:physic}
\end{figure}



where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[$\vec{F_{G}}$:]  Gravity force
 \item[$\vec{F_{N}}$:]  Normal force
 \item[$\vec{F_{R}}$:]  Friction force
 \item[$\vec{F_{Z}}$:]  Centrifugal force
 \item[$\vec{F_{B}}$:]  Gravity force under angle $\alpha$
 \item[$\vec{F_{A}}$:]  Centrifugal force under angle $\alpha$
\end{itemize}


With the above listed parameters we are able to calculate the forces \eqref{eq:f} and can determine the maximum possible speed \eqref{eq:maxspeed}.

\begin{equation}
  \begin{gathered}
    \vec{F_{R}} = \mu \frac{m \cdot g}{\cos{\alpha}} \\
    \vec{F_{Z}} = m \frac{v^{2}}{r} \\
          \vec{F_{G}} = m \cdot g \\
          \vec{F_{A}} = \vec{F_{Z}} \cdot \cos{\alpha} \\
          \vec{F_{B}} = \vec{F_{G}} \cdot \sin{\alpha}
  \end{gathered}\label{eq:f}
\end{equation}



\begin{equation}
  \begin{gathered}
    v_{max} = \sqrt{\frac{r \cdot g (\sin{\alpha} + \mu \cdot \cos{\alpha})}{\cos{\alpha}-\mu \cdot \sin{\alpha}}}
  \end{gathered}\label{eq:maxspeed}
\end{equation}


\paragraph{Example}
To get a better grasp of what these equations represent, let us have a look at an example.
The following are values for the parameters.

\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[] m = \SI{70}{\kilogram}
 \item[] v = \SI[per-mode=fraction]{7}{\metre\per\second} = \SI[per-mode=fraction]{25.2}{\kilo\meter\per\hour}
 \item[] $\mu$ = 0.008 (bicycle tire on rough paved road \cite{rolling})
 \item[] r = \SI{5}{\metre}
 \item[] $\alpha$ = $30 \degree$
 \item[] $g$ = \SI[per-mode=fraction]{9.81}{\metre\per\second\squared}
\end{itemize}

Applying these parameters results in the forces listed in \eqref{eq:physicexample}.

\begin{equation}
  \begin{gathered}
    \vec{F_{Z}} = \SI{70}{\kilogram} \frac{(\SI[per-mode=fraction]{7}{\metre\per\second})^{2}}{\SI{5}{\metre}} = \SI{686}{\newton} \\
    \vec{F_{A}} =  \vec{F_{Z}} \cdot \cos{30 \degree} = \SI{594.1}{\newton} \\
    \vec{F_{R}} = 0.008 \frac{\SI{70}{\kilogram} \cdot \SI[per-mode=fraction]{9.81}{\metre\per\second\squared}}{\cos{30 \degree}} = \SI{6.3}{\newton} \\
    \vec{F_{G}} =  \SI{70}{\kilogram} \cdot \SI[per-mode=fraction]{9.81}{\metre\per\second\squared} = \SI{686.7}{\newton} \\
    \vec{F_{B}} =  \vec{F_{G}} \cdot \sin{30 \degree} = \SI{343.4}{\newton} \\
  \end{gathered}\label{eq:physicexample}
\end{equation}

As you can see in equation \eqref{eq:forcesexample},
force $\vec{F_{A}}$ is bigger than the sum of forces $\vec{F_{B}}$ and $\vec{F_{R}}$.
Thus, the current speed in relation to our example parameters is too high for the curve.

\begin{equation}
  \begin{gathered}
  \vec{F_{A}} >= \vec{F_{B}} + \vec{F_{R}} \\
  \SI{594.1}{\newton} >= \SI{343.4}{\newton} + \SI{6.3}{\newton}
  \end{gathered}\label{eq:forcesexample}
\end{equation}

The related maximal speed could be determined and is shown in \eqref{eq:maxspeedexample}.

\begin{equation}
  \begin{gathered}
      v_{max} = \sqrt{\frac{\SI{5}{\metre} \cdot \SI[per-mode=fraction]{9.81}{\metre\per\second\squared} (\sin{30 \degree} + 0.008 \cdot \cos{30 \degree})}{\cos{30 \degree}- 0.008 \cdot \sin{30 \degree}}}
      = \SI[per-mode=fraction]{5.4}{\metre\per\second} = \SI[per-mode=fraction]{19.4}{\kilo\meter\per\hour}
  \end{gathered}\label{eq:maxspeedexample}
\end{equation}

The comparison between the calculated $v_{max}$ and the current speed $v$ consolidates this result as illustrated in \eqref{eq:vmax}.
\begin{equation}
  \begin{gathered}
      v_{max} < v \\
      \SI[per-mode=fraction]{5.4}{\metre\per\second} < \SI[per-mode=fraction]{7}{\metre\per\second}
  \end{gathered}\label{eq:vmax}
\end{equation}

\subsection{Conclusion}
The chosen physical model fits the circumstances in an adequate way.
It isn't too complex and is calculable in a suitable amount of time even on a smartphone with limited performance.
More problematic are some important parameters like the slope of the curve, the friction coefficient or the skill of the driver.
For example the slope of one curve isn't always the same and highly depends on the exact trail the biker takes.
Further, the friction often changes due to weather or trail usage conditions.
Hence, the Ranck application has an adjustable parameter to counteract these problems.

\newpage

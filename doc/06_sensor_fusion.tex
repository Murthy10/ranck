\section{Sensor fusion}
\label{sec:fusion}
\paragraph{Problem} A sticking point of the application is to determine the current position and the speed of the driver as exactly as possible.
Since the \gls{GPS} signal is not always accurate enough or even worse sometimes completely unavailable, the idea is to combine the multiple sensors of the smartphone.

\paragraph{Goal} To use multiple sensors of the smarthphone to improve the measurement accuracy of the current position and the speed of the biker.

\subsection{Approach}
The chosen procedure in this project was to combine the accelerometer with the \gls{GPS} sensor of the device.
The sensor fusion is realized with a \gls{Kalman filter} \cite{kalman1960new} \cite{simon2001kalman}.

\subsubsection{\gls{Kalman filter}}
The basic idea of a \gls{Kalman filter} \cite{rhudy2017kalman} is to predict the future state of a linear system based on the previous ones.
A linear system is a process described by a state equation \eqref{eq:state} and an output equation \eqref{eq:output}.

\begin{equation}
  \begin{gathered}
    x_{k} = F_{k-1}x_{k-1} + B_{k-1}u_{k-1} + w_{k-1}
  \end{gathered}\label{eq:state}
\end{equation}


\begin{equation}
  \begin{gathered}
    y_{k} = H_{k}x_{k} + v_{k}
  \end{gathered}\label{eq:output}
\end{equation}

where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[x:]  State vector
 \item[y:]  Output vector
 \item[u:]  Input vector
 \item[w:]  Process noise vector
 \item[v:]  Measurement noise vector
 \item[F:]  State transition model matrix
 \item[B:]  Control-input model matrix
 \item[H:]  Observation model matrix
\end{itemize}

After the system is defined, we are able to predict the next measurement value with the following equation \eqref{eq:predict}.

\begin{equation}
  \begin{gathered}
    \hat{x}_{k|k-1} = F_{k-1}\hat{x}_{k-1} + B_{k-1}u_{k-1}
  \end{gathered}\label{eq:predict}
\end{equation}

where:

\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[${\hat{x}_{k|k-1}}$:]  Predicted state vector
 \item[${\hat{x}_{k-1}}$:]  Previous estimated state vector
\end{itemize}

And the state error \gls{covariance} is determined by the equation \eqref{eq:predict}

\begin{equation}
  \begin{gathered}
    P_{k|k-1} = F_{k-1}P_{k-1}F^{T}_{k-1} + Q_{k-1}
  \end{gathered}\label{eq:predict}
\end{equation}

where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[$P_{k|k-1}$:]  Predicted state error \gls{covariance} matrix
 \item[$P_{k-1}$:]  Previous estimated state error \gls{covariance} matrix
 \item[Q:]  Process noise \gls{covariance} matrix
\end{itemize}

With the predicted state error \gls{covariance} matrix, we are able to calculate the Kalman gain \eqref{eq:gain}.

\begin{equation}
  \begin{gathered}
    K_{k} = P_{k|k-1}H^{T}_{k}(H_{k}P_{k|k-1}H^{T}_{k}+R_{k})^{-1}
  \end{gathered}\label{eq:gain}
\end{equation}

where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[H:]  Matrix to define the output equation
 \item[R:]  Measurement noise \gls{covariance}
 \item[K:]  Kalman gain matrix
\end{itemize}

Finally, we can update the estimated state vector \eqref{eq:updatestate} and the state error \gls{covariance} \eqref{eq:updateerror}.

\begin{equation}
  \begin{gathered}
    \hat{x}_{k} = \hat{x}_{k|k-1} + K_{k}(z_{k}-H_{k}\hat{x}_{k|k-1})
  \end{gathered}\label{eq:updatestate}
\end{equation}


\begin{equation}
  \begin{gathered}
    P_{k} = (I-K_{k}H_{k})P_{k|k-1}
  \end{gathered}\label{eq:updateerror}
\end{equation}

where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[$z_{k}$:] Measurement output
 \item[I:]  Identity matrix
\end{itemize}

\newpage
\subsubsection{Sensor fusion model}
As already mentioned, we tried to combine the acceleration sensor and the \gls{GPS} sensor to get a better position and velocity estimation.
This is realized with a \gls{Kalman filter} and the following section describes the used model.

\paragraph{State Vector x} The state vector represents the values that will be estimated by the \gls{Kalman filter}.
In our case this is the position, the velocity and the acceleration.

    \begin{equation}
  \begin{gathered}
 x_{k} =
 \begin{bmatrix}
       x_{p} \\
       y_{p} \\
       x_{v} \\
       y_{v} \\
       x_{a} \\
       x_{a} \\
    \end{bmatrix}
      \end{gathered}\label{eq:appliedstatevector}
\end{equation}

where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[$x_{p}, y_{p}$:] Position
 \item[$x_{v}, y_{v}$:] Velocity
 \item[$x_{a}, y_{a}$:] Acceleration
\end{itemize}

\paragraph{State transition function} The matrix F describes the state transition function which is related to Newton's laws of motion.
\begin{equation}
  \begin{gathered}
 F =
 \begin{bmatrix}
       1 & 0 & \Delta t & 0 & \frac{1}{2}\Delta t^{2} & 0 \\
       0 & 1 & 0 & \Delta t & 0 & \frac{1}{2}\Delta t^{2} \\
       0 & 0 & 1 & 0 & \Delta t & 0 \\
       0 & 0 & 0 & 1 & 0 & \Delta t \\
       0 & 0 & 0 & 0 & 1 & 0 \\
       0 & 0 & 0 & 0 & 0 & 1  \\
    \end{bmatrix}
      \end{gathered}\label{eq:r}
\end{equation}

where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[$\Delta t$:] Time between filter steps
\end{itemize}

\paragraph{Process} The process is described by the equation \eqref{eq:pro}.
The term $Bu$ isn't relevant anymore because there is no control input.
\begin{equation}
  \begin{gathered}
 x_{k} = F_{k-1}x_{k-1}
      \end{gathered}\label{eq:pro}
\end{equation}


\paragraph{Measurment noise \gls{covariance} R} The noise \gls{covariance} matrix R represents the squared noise of the acceleration and the position.
\begin{equation}
  \begin{gathered}
 R =
 \begin{bmatrix}
       r_{p} & 0 & 0 & 0 \\
       0 & r_{p} & 0 & 0 \\
       0 & 0 & r_{a} & 0 \\
       0 & 0 & 0 & r_{a} \\
    \end{bmatrix}
      \end{gathered}\label{eq:r}
\end{equation}

where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[$r_{p}$:] Squared acceleration noise
 \item[$r_{a}$:] Squared position noise
\end{itemize}

\paragraph{Measurement matrix H}
The matrix H determines which states are measurable. In our case this is the acceleration and the position.
\begin{equation}
  \begin{gathered}
 H =
 \begin{bmatrix}
       1 & 0 & 0 & 0 & 0 & 0\\
       0 & 1 & 0 & 0 & 0 & 0\\
       0 & 0 & 0 & 0 & 1 & 0\\
       0 & 0 & 0 & 0 & 0 & 1\\
    \end{bmatrix}
      \end{gathered}\label{eq:h}
\end{equation}

\paragraph{State error \gls{covariance} matrix P}
The matrix P defines the initial uncertainty and is going to be updated during the process.
\begin{equation}
  \begin{gathered}
   P =
\begin{bmatrix}r_{p} & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\0.0 & r_{p} & 0.0 & 0
.0 & 0.0 & 0.0\\0.0 & 0.0 & r_{v} & 0.0 & 0.0 & 0.0\\0.0 & 0.0 & 0.0 & r_{v} & 0.0
 & 0.0\\0.0 & 0.0 & 0.0 & 0.0 & r_{a} & 0.0\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & r_{a}
\end{bmatrix}
      \end{gathered}\label{eq:p}
\end{equation}

where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[$r_{v}$:] Squared velocity noise
\end{itemize}

\paragraph{Process noise \gls{covariance} matrix Q} The matrix Q is related to the process noise.
On our system, the acceleration could be influenced by potholes, wind, stones or other objects on the track.
Thus, the process noise is $w_{k} = Ga_{k}$.
\begin{equation}
  \begin{gathered}
       G =
       \begin{bmatrix}
            \frac{1}{2}\Delta t^{2} & \frac{1}{2}\Delta t^{2} & \Delta t & \Delta t & 1 & 1 \\
          \end{bmatrix}
           \\
 Q = G^{T} G \sigma^{2}_{a}
      \end{gathered}\label{eq:q}
\end{equation}

where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
  \item[G] Vector related to the motion
 \item[$\sigma_{a}$:] Acceleration process noise
\end{itemize}


\subsubsection{Example}
To get an impression of what a \gls{Kalman filter} does, imagine the following circumstances.

We measure the acceleration and position at a fix point and we don't move.
Thus in a world with perfect measurement instruments we would get always the same position and no acceleration at all.
Due to the fact that sensors are imperfect we have some inaccuracy in the values.

Assume the Kalman parameters are:
\begin{equation}
  \begin{gathered}
    \Delta t = 0.1 \\
    r_{p} = 10.0 \\
    r_{v} = 1.0 \\
    r_{a} = 0.25 \\
      \end{gathered}\label{eq:example_param}
\end{equation}

The measured values are generated by a python script. The first three of them are listed in table \ref{tab:experiment_values}.
\begin{table}[H]
\centering
\begin{tabular}{ |l|l|l|l| }
\hline
Position x & -12.93 & 4.01   & 14.2  \\
\hline
Position y & -28.15 & -0.34 & 2.89 \\
\hline
Acceleration x & 0.87  & -0.14  & -0.24  \\
\hline
Acceleration y & -0.36 & 0.65  & -0.51 \\
\hline
\end{tabular}
\caption{Example measurements values}
\label{tab:experiment_values}
\end{table}

Now we are able to set the initial values.

\begin{equation}
  \begin{gathered}
F =
\begin{bmatrix}1.0 & 0.0 & 0.1 & 0.0 & 0.005 & 0.0\\0.0 & 1.0 & 0.0 & 0.1
 & 0.0 & 0.005\\0.0 & 0.0 & 1.0 & 0.0 & 0.1 & 0.0\\0.0 & 0.0 & 0.0 & 1.0 & 0.0
 & 0.1\\0.0 & 0.0 & 0.0 & 0.0 & 1.0 & 0.0\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 1.0
 \end{bmatrix}
, R =
\begin{bmatrix}100.0 & 0.0 & 0.0 & 0.0\\0.0 & 100.0 & 0.0 & 0.0\\0.0 & 0.
0 & 0.25 & 0.0\\0.0 & 0.0 & 0.0 & 0.25\end{bmatrix}\\
P =
\begin{bmatrix}100.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\0.0 & 100.0 & 0.0 & 0
.0 & 0.0 & 0.0\\0.0 & 0.0 & 1.0 & 0.0 & 0.0 & 0.0\\0.0 & 0.0 & 0.0 & 1.0 & 0.0
 & 0.0\\0.0 & 0.0 & 0.0 & 0.0 & 0.25 & 0.0\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.25
\end{bmatrix}
, H =
\begin{bmatrix}1.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\0.0 & 1.0 & 0.0 & 0.0 &
 0.0 & 0.0\\0.0 & 0.0 & 0.0 & 0.0 & 1.0 & 0.0\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 1
.0\end{bmatrix} \\
Q =
 \begin{bmatrix}2.5 \cdot 10^{-5} & 2.5 \cdot 10^{-5} & 0.0005 & 0.0005 &
0.005 & 0.005\\2.5 \cdot 10^{-5} & 2.5 \cdot 10^{-5} & 0.0005 & 0.0005 & 0.005
 & 0.005\\0.0005 & 0.0005 & 0.01 & 0.01 & 0.1 & 0.1\\0.0005 & 0.0005 & 0.01 &
0.01 & 0.1 & 0.1\\0.005 & 0.005 & 0.1 & 0.1 & 1.0 & 1.0\\0.005 & 0.005 & 0.1 &
 0.1 & 1.0 & 1.0\end{bmatrix}
      \end{gathered}\label{eq:example_init_2}
\end{equation}

After the initialization we can apply the \gls{Kalman filter}ing process at the measured values.
This would result in the vector as you can see in \eqref{eq:example_result} for the first three processing steps.
\begin{equation}
  \begin{gathered}
x_{1} =
\begin{bmatrix}-6.47\\-14.08\\0.05\\
-0.02\\0.54\\-0.08\end{bmatrix}
, x_{2} =
\begin{bmatrix}-2.97\\-9.49\\0.07\\0
.03\\0.32\\0.17\end{bmatrix}
, x_{3} =
\begin{bmatrix}1.33\\-6.49\\0.07\\0.
00\\-0.22\\-0.40\end{bmatrix}
      \end{gathered}\label{eq:example_result}
\end{equation}

If we go ahead with this process for another 500 steps
and observe the position, we can see that it goes towards zero in x and zero in y as we expected.
Figure \ref{fig:kalman_positions} shows the results of this process.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth,height=0.8\textheight,keepaspectratio]{images/kalman_positions.png}
    \caption{Verification experiment 1}
    \label{fig:kalman_positions}
\end{figure}

To reproduce the values and the gained result, use the script in \ref{exampleKalmanFilter}.
\newpage
\subsection{Conclusion}
Using of a \gls{Kalman filter} to enhance the measurement accuracy was an adequate approach.
Since it allows the acceleration and the \gls{GPS} values to fusion, it filters out measurement uncertainties and enables to refer to the underlying process.

Nevertheless, a \gls{Kalman filter} does have some pitfalls too. For example, the modulation of a fitting process isn't always obvious.
Time complexity raises in quadratic manner related to the number of parameters \cite{montellaKalman05}.
To handle the noise parameters right you need a lot of experience and if the sensors are not accurate enough, a \gls{Kalman filter} won't help either.

Hence, several attempts to enhance the measurement precision with the combination of the sensors
and the usage of an \gls{Kalman filter} didn't lead to sufficient results.
Due to the fact that the sensors are not sensitive and accurate enough.
Consequently, I decided to use an external bike speed sensor from \gls{Garmin}.

\newpage

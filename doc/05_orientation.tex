\section{Alignment of the accelerometer}
\label{sec:orientation}
\paragraph{Problem} The built-in accelerometer of a smartphone provides measurement values in the directions of the edges.
If you want to combine the \gls{GPS} values and the acceleration, you need to have the same measurement units and the same alignment of the data.
Figure \ref{fig:azimuth_pitch_roll_globe} show the two axis systems.

\begin{figure}[H]
\centering
\subfigure[Globe axis]{\label{fig:axis_globe}\includegraphics[width=0.3\textwidth,height=0.3\textheight,keepaspectratio]{images/axis_globe.png}}
\subfigure[Device axis (\gls{Azimuth}, \gls{Pitch}, \gls{Roll})]{\label{fig:azimuth_pitch_roll}\includegraphics[width=0.4\textwidth,height=0.4\textheight,keepaspectratio]{images/azimuth_pitch_roll.jpg}}
    \caption{Axis systems \cite{matlab_examples}}
    \label{fig:azimuth_pitch_roll_globe}
\end{figure}


\paragraph{Goal} Align the acceleration measurements equal to the \gls{GPS} values.

\subsection{Approach}
To align the acceleration values relative to the Web Mercator (\gls{EPSG}:3857 - \gls{WGS84}) projection, you can use the built-in geomagnetic field and to the gravity sensor.
They enable us to determine the rotation matrix for the right alignment.

The units of the two sensors are micro tesla [\SI[]{}{\micro\tesla}] for the geomagnetic field and metre per second squared [\SI[per-mode=fraction]{}{\metre\per\second\squared}] for the gravity (along the device axises).
Fortunately, we are only interested in the orientation of these forces and not their strength.
Hence, we could normalize the raw values \eqref{eq:normalize}.

The cross product of the gravity and the magnetic north results in a vector pointing to the earth's east.
The cross product of the east pointing vector and the gravity leads to the a vector pointing to the earth's north \eqref{eq:cross_product}.
This enables us to build the rotation matrix out of the gravity of the east pointing vector and the north pointing vector \eqref{eq:rotation_matrix} \cite{simplealgorithms}.

The mathematical equations are shown in \ref{fig:rotation_matrix_math}.
\begin{figure}[H]

\begin{equation}
  \begin{gathered}
 G = \dfrac{1}{|g|}g \\
 M = \dfrac{1}{|m|}m
  \end{gathered}\label{eq:normalize}
\end{equation}

\begin{equation}
  \begin{gathered}
 G \times M = E \\
 E \times G = N \\
  \end{gathered}\label{eq:cross_product}
\end{equation}

\begin{equation}
  \begin{gathered}
 R =
 \begin{bmatrix}
       x_{E} & y_{E} & z_{E} \\
       x_{N} & y_{N} & z_{N} \\
       x_{G} & y_{G} & z_{G} \\
    \end{bmatrix}
      \end{gathered}\label{eq:rotation_matrix}
\end{equation}

where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[g:]  Raw values from the gravity sensor
 \item[m:]  Raw values from the magnetic field sensor
 \item[G:]  Normalized gravity vector
 \item[M:]  Normalized magnetic field vector
 \item[E:]  East vector
 \item[N:]  North vector
 \item[R:]  Rotation matrix
\end{itemize}

    \caption{equations to determine the rotation matrix}
    \label{fig:rotation_matrix_math}
\end{figure}

To get a better understanding of these equations, let's have a look at an example.
Assume we have the following measurement values \eqref{eq:gravity_magnetic}:

\begin{equation}
  \begin{gathered}
    g = \begin{bmatrix}
               -5.76  \\
                7.17  \\
               -3.42  \\
            \end{bmatrix}
,
    m = \begin{bmatrix}
                   19.38  \\
                    -15.54  \\
                   41.58  \\
                \end{bmatrix}
  \end{gathered}\label{eq:gravity_magnetic}
\end{equation}

Now we are able to calculate the normalized vectors $G$ and $M$ \eqref{eq:normalized}.
\begin{equation}
  \begin{gathered}
    G = \frac{1}{\sqrt{-5.76^2 + 7.17^2 + -3.42^2}} \cdot \begin{bmatrix}
                                                                       -5.76  \\
                                                                        7.17  \\
                                                                       -3.42  \\
                                                                    \end{bmatrix}
      = \begin{bmatrix}
                                                                               -0.59  \\
                                                                                0.73  \\
                                                                               -0.35  \\
                                                                            \end{bmatrix} \\
    M = \frac{1}{\sqrt{19.38^2 + -15.54^2 + -41.58^2}} \cdot \begin{bmatrix}
                                                                           19.38  \\
                                                                            -15.54   \\
                                                                           41.58  \\
                                                                        \end{bmatrix}
    = \begin{bmatrix}
                                                                                0.40  \\
                                                                                 -0.32   \\
                                                                                0.86  \\
                                                                             \end{bmatrix}
  \end{gathered}\label{eq:normalized}
\end{equation}

The corresponding vectors pointing to the earth's east and north can be computed as you can see at in equation \eqref{eq:crossproduct}.

\begin{equation}
  \begin{gathered}
    E = \begin{bmatrix}
      -0.59  \\
      0.73  \\
      -0.35  \\
      \end{bmatrix} \times
   \begin{bmatrix}
     0.40  \\
     -0.32   \\
     0.86  \\
   \end{bmatrix}  =
   \begin{bmatrix}
     0.52  \\
     0.36  \\
     -0.10  \\
    \end{bmatrix}    \\
    N =
    \begin{bmatrix}
     0.52  \\
     0.36  \\
     -0.10  \\
     \end{bmatrix} \times
    \begin{bmatrix}
      -0.59  \\
      0.73  \\
      -0.35  \\
      \end{bmatrix}
     =
    \begin{bmatrix}
      -0.05  \\
      0.24  \\
      0.59  \\
      \end{bmatrix}
  \end{gathered}\label{eq:crossproduct}
\end{equation}

Figure \ref{fig:rotation_matrix} illustrates the determined vectors.


\begin{figure}[H]
\centering
\subfigure[Gravity (blue), Magnetic (black), East (red)]{\label{fig:gravity_magnetic_east}\includegraphics[width=0.4\textwidth,height=0.4\textheight,keepaspectratio]{images/gravity_blue_magnetic_black_east_red_croped.png}}
\subfigure[East (red), Gravity (blue), north (green)]{\label{fig:east_gravity_north}\includegraphics[width=0.4\textwidth,height=0.4\textheight,keepaspectratio]{images/gravity_blue_east_red_north_green_croped.png}}
    \caption{Cross products of the gravity and magnetic field sensor values resulting in the components for the rotation matrix}
    \label{fig:rotation_matrix}
\end{figure}

\newpage
\subsection{Verification}
To verify if the projection from the device axis aligned acceleration values to the Web Mercator (\gls{EPSG}:3857 - \gls{WGS84}) spatial reference system works as expected, two experiments were carried out.

\subsubsection{Experiment 1}
\paragraph{Construction}
The first experiment looks at the processed acceleration values (they should be relative to the Web Mercator projection).
The acceleration in the z-axis that should result to the earth's gravity is particularly important.
For that purpose, the smartphone is in a fix position (with indefinite rotation) illustrated in figure \ref{fig:acceleration_verification_1}.
Hence, the processed acceleration should approximately be \SI[per-mode=fraction]{0.0}{\metre\per\second\squared} in the x-axis, \SI[per-mode=fraction]{0.0}{\metre\per\second\squared} in the y-axis and \SI[per-mode=fraction]{9.81}{\metre\per\second\squared} in the z-axis (gravity acceleration of the earth).
Figure \ref{fig:acceleration_verification_1} represents the construction of the experiment.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth,height=0.5\textheight,keepaspectratio]{images/azimuth_pitch_roll_relativ_to_earth.png}
    \caption{Verification experiment 1}
    \label{fig:acceleration_verification_1}
\end{figure}

\paragraph{Results} The tracked values of the raw acceleration and the processed ones verify the expected values.
An excerpt of the measurements is shown in the table \ref{tab:experiment_1}.

\begin{table}[H]
\centering
\begin{tabular}{ |l|l|l|l|l|l| }
\hline
\multicolumn{3}{| l |}{Raw acceleration} & \multicolumn{3}{| l |}{Processed acceleration}  \\
\hline
x & y & z & x & x & z \\
\hline
-6.160 & 7.185 & 3.235 & -0.021 & -0.094 & 10.001 \\
-6.153 & 7.204 & 3.220 & -0.042 & -0.106 & 10.006 \\
-6.174 & 7.170 & 3.356 & -0.023 & -0.015 &  10.041 \\
-6.174 & 7.173 & 3.416 & -0.033 & -0.069 & 10.062 \\
\hline
\end{tabular}
\caption{Measurements experiment 1}
\label{tab:experiment_1}
\end{table}

\paragraph{Conclusion} As you can see the values do have a certain shift related to measuring inaccuracies.
To avoid this problem, an initial calibration should be done.


\subsubsection{Experiment 2}
\paragraph{Construction} The second experiment observes the orientation of the acceleration of the x and y axises.
The idea is to move the smartphone along a straight line, once in a vertical and once in a horizontal position, see figure \ref{fig:experiment_setup}.
The expected values of the raw acceleration should differ in direction,
but the processed values (relative to Web Mercator projection) should point in the same direction independently of the device orientation.


\begin{figure}[H]
\centering     %%% not \center
\subfigure[horizontal]{\label{fig:horizontal}\includegraphics[width=0.3\textwidth,height=0.3\textheight,keepaspectratio]{images/smartphone_horizontal.png}}
\subfigure[vertical]{\label{fig:vertical}\includegraphics[width=0.3\textwidth,height=0.3\textheight,keepaspectratio]{images/smartphone_vertical.png}}
\caption{Experiment 2 visualization}
\label{fig:experiment_setup}
\end{figure}

\paragraph{Results} The logged values of the raw acceleration and processed ones verify the expected results.
As you can see in figure \ref{fig:acceleration_verification_2} the processed values of both measurements
(Vertical Acceleration Earth relative and Horizontal Acceleration Earth relative) point to the same direction,
whereas the different directions are clearly recognizable between the Vertical acceleration and Vertical Acceleration Earth relative.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth,height=0.6\textheight,keepaspectratio]{images/acceleration.png}
    \caption{Verification experiment 2}
    \label{fig:acceleration_verification_2}
\end{figure}

\paragraph{Conclusion} The logged values verify the correctness of the reorienting of the acceleration,
but also uncover some uncertainties in the measurement values.


\subsection{Conclusion}
The basic information to align the acceleration measurements relative to Web Mercator spatial reference system is projectable by the gravity
and the magnetic field sensor of the smartphone.
Furthermore, the Android SDK provides the functions to transform the values into the needed projection.
Unfortunately, there are some pitfalls with that process like the sensor scattering \cite{fsensor}.
The magnetic sensor could be influenced by other magnetic fields.
In addition, there is the problem of measuring the gravity when the device is in motion \cite{rotationmatrixandroid}.

\newpage


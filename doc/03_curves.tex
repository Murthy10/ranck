\section{Curves}
\label{curves}
\paragraph{Problem} The tracks appear in line strings of coordinates, there is no information about when a curve starts or ends,
the radius, the direction or even what a curve is.
Thus, we have to figure out how to split the tracks into reasonable parts and figure out how a curve could be represented.

\paragraph{Goal} To build segments out of the tracks, calculate the radius of the curves, define their directions and find an adequate representation for the tracks and curves.
Furthermore, the process should be repeatable and comprehensible.


\subsection{Approach}
To define the track segments, two processing steps were taken.
First we calculate the radius and the direction for every point on the track \cite{roadcurvature}.
The radius is computable out of three points illustrated in figure \ref{fig:curve}.
The direction is determined by the cross product of three points.
The chosen format to represent these tracks is \gls{GeoJSON}, since it is easy to understand,
readable with every text editor and provides a property field to enhance the data in accordance to your wishes.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.4\textwidth,height=0.4\textheight,keepaspectratio]{images/curve.png}
    \caption{Radius out of three points}
    \label{fig:curve}
\end{figure}

The second step is to build segments from the points of the line string.
The algorithm summarizes all points along the track until the radius leaves a certain range or the directions change.
The ranges depend on the radius and are listed in table \ref{tab:curve_ranges}.
After a change, a mean radius is calculated out of all points in the current segment.
To store the gained information, a properties field is added to the \gls{GeoJSON} file
with the mean radius and the direction for every point of the track.
Thus, if you go along the properties of the track, you know a new segment begins should the radius or the direction change.

\begin{table}[H]
\centering
\begin{tabular}{ |l|l|l| }
\hline
Radius range [\SI[]{}{\metre}] \ & Category & Name \\
\hline
0-10 & 1 & small curve \\
10-40 & 2 & curve \\
40-$\infty$ & 3 & straight \\
\hline
\end{tabular}
\caption{Curve ranges}
\label{tab:curve_ranges}
\end{table}


\paragraph{Example}
To visualize the algorithm, have a look at figure \ref{fig:track_segments_visualization}.
On the left-hand side, the raw track is represented
and on the other side there is the processed track with a color for every segment that belongs together.
The website geojson.io \cite{geojson} provides a tool to create geometries represented as \gls{GeoJSON}.
This tool was used to generate the tracks for the Ranck application.

\begin{figure}[H]
\centering     %%% not \center
\subfigure[track]{\label{fig:track}\includegraphics[width=0.49\textwidth,height=0.49\textheight,keepaspectratio]{images/track_visualisation_croped.png}}
\subfigure[track segments]{\label{fig:track_segments}\includegraphics[width=0.49\textwidth,height=0.49\textheight,keepaspectratio]{images/track_visualisation_segments_croped.png}}
\caption{Track segment visualization}
\label{fig:track_segments_visualization}
\end{figure}

Listing \ref{geojson} shows the related line string in the \gls{GeoJSON} format.
As you can see, there are the additional properties directions and radiuses. \\

\begin{lstlisting}[label=geojson, language=iPython, caption={\gls{GeoJSON} example},captionpos=b]
{
"features": [{
  "properties": {
    "directions": [
            -1.0, -1.0, -1.0, -1.0, -1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0, 1.0, -1.0
      ],
    "radiuses": [
            11.679104489078362, 11.679104489078362,
	    11.679104489078362, 7.612380836338962,
	    35.98007719302654, 44.87616608734529,
	    22.04574453395551, 22.04574453395551,
	    54.83334336476179, 20.41688307583868,
	    20.41688307583868, 20.41688307583868,
	    1135.0356087700316
	  ]
  },
  "geometry": {
    "type": "LineString",
    "coordinates": [
          [9.335168302059174, 47.11897340210018],
	  [9.335152208805084, 47.11894054876184],
	  [9.335120022296906, 47.118918646525],
	  [9.335061013698578, 47.118918646525],
	  [9.334993958473206, 47.118927772458136],
	  [9.334883987903595, 47.11892594727162],
	  [9.334830343723297, 47.11890039465411],
	  [9.334782063961029, 47.11885841532722],
	  [9.334712326526642, 47.11874707869123],
	  [9.334723055362701, 47.11871422521314],
	  [9.334792792797089, 47.118679546519814],
	  [9.334940314292908, 47.118661294566884],
	  [9.335050284862518, 47.118646693000024]
	]
  },
  "type": "Feature"
}],
"type": "FeatureCollection"
}
\end{lstlisting}

\paragraph{Result} The described process resulted in a repository on \cite{GitLab} called track\_processing \cite{trackprocessing}.
The scope of application is not only restricted to the Ranck app. Other similar applications could benefit from these findings too.

\subsection{Conclusion}
The current approach to split the track into accurate segments fulfills the requirements.
With the use of the \gls{GeoJSON} format, a simple representation of the tracks is chosen with the possibility to add extended information.
In addition there are several libraries that facilitate the handling of the format \cite{shapely} \cite{jts2geojson}.
Creating the tracks by hand is time-consuming.
To avoid this toil, further steps could include recording the tracks during the rides
and automatically generating the processed tracks.
\newpage

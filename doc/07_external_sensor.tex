\section{External sensor}
\label{sec:external}
\paragraph{Problem} The built-in sensors of the smartphone are not accurate enough to handle fast speed changes and to determine an adequate position.

\paragraph{Goal} To improve the measurements to get sufficient position and speed values that will enable an suitable feedback for the biker.

\paragraph{Decision} To improve the measurement accuracy, I used an external bike speed sensor from \gls{Garmin} since they provide an \gls{API} to the measurements directly from the smartphone.

\subsection{Approach}
The bike speed sensor from \gls{Garmin} uses \gls{ANT}+ technology to communicate with the smartphone.
There is a instruction in how to access the sensor data and integrate it in your own Android application \cite{antdevelop}.

\subsubsection{Installation}
The basic steps to install and start using the \gls{ANT}+ are:
\begin{enumerate}
    \item Download the Android \gls{ANT}+ SDK to communicate with \gls{ANT}+ devices \cite{antdownload}.
    \item Add the SDK (antpluginlib\_x-y-z.jar) to your Android Application libraries.
    \item Install the \gls{ANT} Radio Service App on your Android smartphone \cite{radioservice}.
    \item Install the \gls{ANT}+ Plugins Service App on your Android smartphone \cite{antplugins}.
\end{enumerate}

Unfortunately, not all smartphones support the \gls{ANT}+ protocol.
To get an overview of all supporting smartphones, consider the list of compatible devices \cite{devices}.

\subsubsection{Development}
The downloaded Android \gls{ANT}+ SDK provides a document called "Creating \gls{ANT}+ Android Applications".
This document gives a brief overview and starting point of the \gls{ANT}+ development with Android.

The procedure to use the \gls{ANT}+ \gls{API} is described as the following:
\begin{enumerate}
    \item Search for devices
    \item Request access to the plugin
    \item Subscribe to events
    \item Use device while monitoring the device connection state
    \item Release the plugin communicator objects (called \gls{PCC}s)
\end{enumerate}

The documentation is not very detailed, luckily there are sample Android applications \cite{antsample} and  \gls{API} documentation \cite{antjavadoc}.
To get more familiar with this procedure, let us take a look at some code snippets.


\subsubsection{Search for devices}
To get the devices you are interested in,
you could use the class MultiDeviceSearch that takes the application context,
a set of devices and a callback as parameters. An example of the device search is shown in listing \ref{searchForDevices}.
If a device is found, you have to ask for access as you can see in listing \ref{requestDeviceAccess}.

\begin{lstlisting}[label=searchForDevices, language=myJava, caption={Search for device listing},captionpos=b]
private void searchForDevices() {
        EnumSet<DeviceType> deviceTypes = EnumSet.of(DeviceType.BIKE_SPD);
        MultiDeviceSearch.SearchCallbacks searchCallbacks =
                                          new MultiDeviceSearch.SearchCallbacks() {

            @Override
            public void onSearchStarted(MultiDeviceSearch.RssiSupport rssiSupport) {
            }

            @Override
            public void onDeviceFound(MultiDeviceSearchResult multiDeviceSearchResult) {
                mMultiDeviceSearchResult = multiDeviceSearchResult;
                requestDeviceAccess(mMultiDeviceSearchResult);
            }

            @Override
            public void onSearchStopped(RequestAccessResult requestAccessResult) {
            }
        };
        MultiDeviceSearch _ =
                          new MultiDeviceSearch(context, deviceTypes, searchCallbacks);
    }
\end{lstlisting}

\subsubsection{Request access to the plugin}
To get access, you could us the requestAccess method of the \gls{PCC} object, concerned.
Previously, you should check that you are using the sensor of your choice.
An example is represented in listing \ref{requestDeviceAccess}.

\begin{lstlisting}[label=requestDeviceAccess, language=myJava, caption={Request access listing},captionpos=b]
    private void requestDeviceAccess(MultiDeviceSearchResult multiDeviceSearchResult) {
        boolean isBSC = multiDeviceSearchResult.getAntDeviceType()
                        .equals(DeviceType.BIKE_SPDCAD);
        AntPlusBikeSpeedDistancePcc.requestAccess(context,
                                    multiDeviceSearchResult.getAntDeviceNumber(), 0,
                                    isBSC, mResultReceiver, mDeviceStateChangeReceiver);
    }
\end{lstlisting}


\subsubsection{Subscribe to events}
The \gls{PCC} objects allow to subscribe to events of the sensors.
In listing \ref{subscribeToEvents} we use a AntPlusBikeSpeedDistancePcc related to our Bike Speed Sensor.
This sensor needs the wheel circumference to calculate the speed.
If a new value is provided by the sensor the onNewCaculatedSpeed method is called and you are able use the value for your needs.

\begin{lstlisting}[label=subscribeToEvents, language=myJava, caption={Subscribe to events listing},captionpos=b]
    private void subscribeToEvents() {
        BigDecimal wheelCircumference = new BigDecimal(2.095) ; //average road tire
        AntPlusBikeSpeedDistancePcc.CalculatedSpeedReceiver speedReceiver =
            new AntPlusBikeSpeedDistancePcc.CalculatedSpeedReceiver(wheelCircumference)

        bsdPcc.subscribeCalculatedSpeedEvent(speedReceiver) {
            @Override
            public void onNewCalculatedSpeed(final long esTimestamp,
                                             final EnumSet<EventFlag> eventFlags,
                                             final BigDecimal calculatedSpeed) {
                setSpeed(calculatedSpeed.doubleValue());
            }
        });
    }
\end{lstlisting}



\subsubsection{Use device while monitoring the device connection state}
Device state changes could be handle by the DeviceStateChangeReceiver as displayed in listing \ref{monitorDeviceStateChanges}.
The handling of the measurements is done by a callback as shown in listing \ref{subscribeToEvents}.

\begin{lstlisting}[label=monitorDeviceStateChanges, language=myJava, caption={Monitoring the device connection state listing},captionpos=b]
    private void monitorDeviceStateChanges(){
        mDeviceStateChangeReceiver = new AntPluginPcc.IDeviceStateChangeReceiver() {
            @Override
            public void onDeviceStateChange(final DeviceState newDeviceState) {
                Log.i(TAG, bsdPcc.getDeviceName() + ": " + newDeviceState);
                if (newDeviceState == DeviceState.DEAD) bsdPcc = null;
            }
        };
    }
\end{lstlisting}


\subsubsection{Release the plugin communicator objects}
Finally, if you don't need the sensor data anymore, you could close the communication with the PccReleaseHandle.
Illustrated in listing \ref{releaseTheCommunicatorObject}.

\begin{lstlisting}[label=releaseTheCommunicatorObject, language=myJava, caption={Release the plugin communicator objects listing},captionpos=b]
    private void destroy() {
        bsdReleaseHandle.close();
        if (bcReleaseHandle != null) {
            bcReleaseHandle.close();
        }
    }
\end{lstlisting}




\subsection{Conclusion}
The \gls{Garmin} Bike speed sensor worked as expected and delivered accurate measurements.
Due to the fact that the sensor only detects rotations
and you have to set the circumference of the wheel on your own, measurement inaccuracies could appear.
For example there could be more or less air in the tire.
The documentation of the \gls{ANT} \gls{API} for developing an Android app was not very detailed.
Furthermore, not all smartphones support the \gls{ANT} protocol.
Therefore, I had to change my development device from an Nexus X5 \cite{nexus} to a Samsung Galaxy S8 \cite{s8}.
Even with the adequate speed measurements, the exact position on the track is still missing.

It would be possible to use further external sensors to improve the measurement accuracy but this would have exceeded the scope of this project.
Unfortunately, the more the external sensors are necessary the less likely it is that the Ranck app be used.


\newpage

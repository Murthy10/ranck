package com.murthy.ranck.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.function.Consumer;

public class RanckReceiver extends BroadcastReceiver {

    private final String TAG = this.getClass().getSimpleName();

    private Consumer<double[]> callback;

    @Override
    public void onReceive(Context context, Intent intent) {
        double[] values = intent.getDoubleArrayExtra("Ranck");
        callback.accept(values);
    }


    public void setCallback(Consumer<double[]> callback) {
        this.callback = callback;
    }

}

package com.murthy.ranck.services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.murthy.ranck.R;
import com.murthy.ranck.physics.SpeedChecker;
import com.murthy.ranck.sensors.AccelerationSensor;
import com.murthy.ranck.sensors.BikeSpeedSensor;
import com.murthy.ranck.sensors.GpsSensor;
import com.murthy.ranck.tracks.Curve;
import com.murthy.ranck.tracks.Track;
import com.murthy.ranck.tracks.TrackReader;
import com.murthy.ranck.utils.LocationConverter;
import com.murthy.ranck.utils.LocationEstimator;

import org.apache.commons.math3.util.Precision;

import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class RanckService extends Service {
    private final String TAG = this.getClass().getSimpleName();
    public static String RANCK_SERVICE = "com.murthy.ranck.services.ranck_service";
    private SharedPreferences mPreferences;

    private BikeSpeedSensor mBikeSpeedSensor;
    private GpsSensor mGpsSensor;
    private AccelerationSensor accelerationSensor;
    private boolean mKalmanIsInit;
    private SpeedChecker mSpeedChecker;
    private Track mTrack;
    private LocationEstimator mLocationEstimator;
    private double mCurrentSpeed; // Meter per second
    private double[] mCurrentPosition; // Meters
    private double[] mLastKnownPosition;
    private LocationConverter mLocationConverter;
    private int mInterval; //Milliseconds
    private int mDelay; // Milliseconds

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mInterval = 200;
        mDelay = 5000;
        mPreferences = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        initBikeSpeedSensor();
        initSpeedChecker();
        initTrack();
        initImu();
        //accelerationSensor = new AccelerationSensor(this.getApplicationContext());
        return Service.START_NOT_STICKY;
    }

    private void initSpeedChecker() {
        mSpeedChecker = new SpeedChecker(this.getApplicationContext());
    }

    private void initImu() {
        mGpsSensor = new GpsSensor(this.getApplicationContext());
        setCurrentSpeed(new double[]{0., 0.});
        mCurrentPosition = new double[]{0., 0.};
        mLocationConverter = new LocationConverter();
        initTimer();
    }

    private void initTrack() {
        TrackReader trackReader = new TrackReader();
        mTrack = trackReader.getTrack(this.getApplicationContext());
        mLocationEstimator = new LocationEstimator(mTrack, mInterval);
        mLastKnownPosition = mTrack.getFistCoordinate();
    }

    private void initBikeSpeedSensor() {
        mBikeSpeedSensor = new BikeSpeedSensor(this.getApplicationContext());
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void publish(double[] values) {
        mCurrentPosition = new double[]{values[0], values[1]};
        if (useBikeSpeedSensor()) {
            mCurrentSpeed = mBikeSpeedSensor.getSpeed();
        } else {
            setCurrentSpeed(new double[]{values[2]});
        }
        if (gpsAccuracyToLow()) {
            mCurrentPosition = mLocationEstimator.estimateLocation(mCurrentSpeed, mLastKnownPosition);
        } else {
            mLastKnownPosition = mCurrentPosition;
            mLocationEstimator.resetEstimation();
        }
        Curve curve = getCurve();
        //Log.i(TAG, "Radius: " + curve.getRadius() + " Direction: " + curve.getDirection());
        //Log.i(TAG, "GPS: " + mGpsSensor.getLocation().getLatitude() + "," + mGpsSensor.getLocation().getLongitude());
        //Log.i(TAG, "GPS accuracy: " + gpsAccuracyToLow());
        double maxSpeed = getMaxSpeed(curve.getRadius());
        double status = mSpeedChecker.status(mCurrentSpeed, maxSpeed);
        Intent ranckIntent = new Intent(RANCK_SERVICE).putExtra("Ranck", new double[]{getSpeedInKmH(mCurrentSpeed), curve.getDirection(), curve.getRadius(), status});
        sendBroadCast(ranckIntent);
    }


    public double getMaxSpeed(double radius) {
        //TODO Add friction and slop to geojson
        double friction = 0.5;
        double slope = 5;
        return mSpeedChecker.maxSpeed(radius, friction, slope);
    }

    private void sendBroadCast(Intent intent) {
        LocalBroadcastManager.getInstance(RanckService.this).sendBroadcast(intent);
    }

    private double getSpeedInKmH(double speed) {
        return Precision.round((speed * 3.6), 1);
    }

    private void setCurrentSpeed(double[] velocity) {
        mCurrentSpeed = calculateVelocityFromVector(velocity);
    }

    private double calculateVelocityFromVector(double[] values) {
        return sqrt(Arrays.stream(values).map(d -> pow(d, 2)).sum());
    }

    private Curve getCurve() {
        return mTrack.getNextCurve(mCurrentPosition[0], mCurrentPosition[1]);
    }

    private boolean useBikeSpeedSensor() {
        return mPreferences.getBoolean(getString(R.string.ant_speed_sensor_key), false);
    }

    private void initTimer() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                process();
            }
        };
        timer.scheduleAtFixedRate(task, mDelay, mInterval);
    }

    private void process() {
        if (mGpsSensor.isReady()) {
            double[] llaLocation = mGpsSensor.getLocationXYZ();
            double[] webMercatorLocation = this.getWebMercatorLocation(llaLocation);
            double speed = mGpsSensor.getCurrentSpeed();
            publish(new double[]{webMercatorLocation[0], webMercatorLocation[1], speed});
        }
    }

    private boolean gpsAccuracyToLow() {
        return mGpsSensor.getAccuracy() > getGpsAccuracyLimit();
    }

    public double getGpsAccuracyLimit() {
        return mPreferences.getInt(this.getApplication().getString(R.string.gps_accuracy_key), 30);
    }

    public double[] getWebMercatorLocation(double[] location) {
        return mLocationConverter.llaToWebMercator(new double[]{location[0], location[1]});
    }
}

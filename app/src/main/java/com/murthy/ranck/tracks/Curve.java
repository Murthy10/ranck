package com.murthy.ranck.tracks;


public class Curve {

    private final double radius;
    private final double direction;

    public Curve(double radius, double direction) {
        this.radius = radius;
        this.direction = direction;
    }

    public double getRadius() {
        return radius;
    }

    //0 = forward, 1.0 = left, -1.0 = right
    public double getDirection() {
        if (radius >= 75.0) {
            return 0.0;
        }
        return direction;
    }


    /*
    CATEGORIES = {
        range(0, 3): {'category': 1, 'name': 'very small curve'},
        range(3, 8): {'category': 2, 'name': 'small curve'},
        range(8, 15): {'category': 3, 'name': 'curve'},
        range(15, 30): {'category': 4, 'name': 'big curve'},
        range(30, 50): {'category': 5, 'name': 'ver big curve'},
        range(50, sys.maxsize): {'category': 6, 'name': 'straight'},
     }
     */
}

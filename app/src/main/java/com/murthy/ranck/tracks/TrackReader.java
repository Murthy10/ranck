package com.murthy.ranck.tracks;


import android.content.Context;
import android.util.Log;

import com.murthy.ranck.R;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;

import org.wololo.geojson.Feature;
import org.wololo.geojson.FeatureCollection;
import org.wololo.geojson.GeoJSONFactory;
import org.wololo.jts2geojson.GeoJSONReader;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TrackReader {
    private final String TAG = this.getClass().getSimpleName();

    public Track getTrack(Context context) {
        String jsonString = readJSON(context);
        FeatureCollection featureCollection = (FeatureCollection) GeoJSONFactory.create(jsonString);
        GeoJSONReader reader = new GeoJSONReader();
        List<Double> radiusList = new ArrayList<>();
        List<Double> directionList = new ArrayList<>();
        List<Geometry> geometries = new ArrayList<>();
        for (Feature feature : featureCollection.getFeatures()) {
            Geometry geometry = reader.read(feature.getGeometry());
            geometries.add(geometry);
            Map<String, Object> properties = feature.getProperties();
            radiusList = (ArrayList<Double>) properties.get("radiuses");
            directionList = (ArrayList<Double>) properties.get("directions");
        }
        GeometryFactory geometryFactory = new GeometryFactory();
        LineString lineString = geometryFactory.createLineString(geometries.get(0).getCoordinates());
        double[] radiuses = radiusList.stream().mapToDouble(d -> d).toArray();
        double[] directions = directionList.stream().mapToDouble(d -> d).toArray();
        return new Track(lineString, radiuses, directions);
    }

    private String readJSON(Context context) {
        String jsonString = "";
        try {
            InputStream is = context.getResources().openRawResource(R.raw.track);
            byte[] b = new byte[is.available()];
            is.read(b);
            jsonString = new String(b);
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        }
        return jsonString;
    }
}

package com.murthy.ranck.tracks;

import android.util.Log;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.linearref.LinearIterator;


public class Track {
    private final String TAG = this.getClass().getSimpleName();
    private LineString mLineString;
    private double[] radiuses;
    private double[] directions;
    private GeometryFactory geometryFactory;


    public Track(LineString lineString, double[] radiuses, double[] directions) {
        this.mLineString = lineString;
        this.radiuses = radiuses;
        this.directions = directions;

    }

    public int getNearestCoordinateIndex(Coordinate coordinate) {
        double distance = Double.MAX_VALUE;
        int index = -1;
        for (LinearIterator it = new LinearIterator(mLineString); it.hasNext(); it.next()) {
            int vi = it.getVertexIndex();
            Coordinate currentCoordinate = mLineString.getCoordinateN(vi);
            double currentDistance = currentCoordinate.distance(coordinate);
            if (currentDistance < distance) {
                index = vi;
                distance = currentDistance;
            }
        }
        return index;
    }

    public int getIndexOfCoordinate(Coordinate coordinate) {
        int index = -1;
        for (LinearIterator it = new LinearIterator(mLineString); it.hasNext(); it.next()) {
            int vi = it.getVertexIndex();
            Coordinate currentCoordinate = mLineString.getCoordinateN(vi);
            if (coordinate.equals2D(currentCoordinate)) {
                index = vi;
                break;
            }
        }
        return index;
    }

    public Coordinate getNearestCoordinate(Coordinate coordinate) {
        int nearestIndex = getNearestCoordinateIndex(coordinate);
        return mLineString.getCoordinateN(nearestIndex);
    }

    public Coordinate getNearestCoordinate(double x, double y) {
        Coordinate coordinate = new Coordinate(x, y);
        return getNearestCoordinate(coordinate);
    }

    public Coordinate getCoordinateByIndex(int index) {
        if (index >= mLineString.getNumPoints() - 1)
            return mLineString.getCoordinateN(mLineString.getNumPoints() - 1);
        if (index < 0) return mLineString.getCoordinateN(0);
        return mLineString.getCoordinateN(index);
    }

    private Curve getNextCurve(int index) {
        double currentRadius = radiuses[index];
        for (int i = index; i < radiuses.length; i++) {
            if (radiuses[i] != currentRadius) {
                return new Curve(radiuses[i], directions[i]);
            }
        }
        return new Curve(radiuses[index], directions[index]);
    }

    private Curve getCurrentCurve(int index) {
        return new Curve(radiuses[index], directions[index]);
    }

    public Curve getNextCurve(double x, double y) {
        Coordinate coordinate = new Coordinate(x, y);
        return getNextCurve(coordinate);
    }

    public Curve getNextCurve(Coordinate coordinate) {
        int nearestCoordinateIndex = getNearestCoordinateIndex(coordinate);
        double distanceNextCurve = distanceToNextCurve(nearestCoordinateIndex);
        double distancePreviousCurve = distanceToPreviousCurve(nearestCoordinateIndex);
        if (distanceNextCurve <= distancePreviousCurve) {
            return getNextCurve(nearestCoordinateIndex);
        } else {
            return getCurrentCurve(nearestCoordinateIndex);
        }
    }

    public double[] getFistCoordinate() {
        Coordinate first = getCoordinateByIndex(0);
        return new double[]{first.x, first.y};
    }

    public int numberOfPoints() {
        return mLineString.getNumPoints();
    }

    public double distanceToNextCurve(int index) {
        double distance = 0.;
        for (int i = index; i < radiuses.length - 1; i++) {
            Coordinate current = mLineString.getCoordinateN(i);
            Coordinate next = mLineString.getCoordinateN(i + 1);
            distance += current.distance(next);
            if (radiuses[i] != radiuses[i + 1]) {
                return distance;
            }
        }
        return distance;
    }

    public double distanceToPreviousCurve(int index) {
        double distance = 0.;
        for (int i = index; i >= 1; i--) {
            Coordinate current = mLineString.getCoordinateN(i);
            Coordinate previous = mLineString.getCoordinateN(i - 1);
            distance += current.distance(previous);
            if (radiuses[i] != radiuses[i - 1]) {
                return distance;
            }
        }
        return distance;
    }

}

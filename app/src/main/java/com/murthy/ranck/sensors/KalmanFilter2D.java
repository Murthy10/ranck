package com.murthy.ranck.sensors;


import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class KalmanFilter2D {
    private final String TAG = this.getClass().getSimpleName();
    public RealMatrix x;
    public RealMatrix P;
    public RealMatrix A;
    public RealMatrix H;
    public RealMatrix R;
    public RealMatrix Q;
    public RealMatrix I;
    private double dt; // Time Step between Filter Steps

    public KalmanFilter2D(double dt) {
        init(dt);
    }

    private void init(double dt) {
        //[ x-Position, y-Position, x-Velocity, y-Velocity, x-AccelerationSensor, y-AccelerationSensor]
        this.dt = dt;
        x = new Array2DRowRealMatrix(new double[][]{{0.0, 0.0, 0.0, 0.0, 0.0, 0.0}}).transpose();

        //uncertainty
        double noisePosition = 10.0;
        double noiseVelocity = 3.0;
        double noiseAcceleration = 7.0;
        P = initP(noisePosition, noiseVelocity, noiseAcceleration);

        //Dynamic Matrix
        A = initA();

        // Measurement Matrix
        H = initH();

        // AccelerationSensor Measurement Noise Covariance
        R = initR(noisePosition, noiseAcceleration);

        // Process Noise Covariance Matrix
        Q = initQ();

        // Identity Matrix
        I = MatrixUtils.createRealIdentityMatrix(6);
    }

    private RealMatrix initP(double noisePosition, double noiseVelocity, double noiseAcceleration) {
        return MatrixUtils.createRealDiagonalMatrix(new double[]{noisePosition, noisePosition, noiseVelocity, noiseVelocity, noiseAcceleration, noiseAcceleration});
    }


    private RealMatrix initA() {
        double a = 1.0;
        double s = 0.5 * Math.pow(dt, 2);
        return new Array2DRowRealMatrix(new double[][]{
                {a, 0.0, dt, 0.0, s, 0.0},
                {0.0, a, 0.0, dt, 0.0, s},
                {0.0, 0.0, a, 0.0, dt, 0.0},
                {0.0, 0.0, 0.0, a, 0.0, dt},
                {0.0, 0.0, 0.0, 0.0, a, 0.0},
                {0.0, 0.0, 0.0, 0.0, 0.0, a},
        });
    }


    private RealMatrix initH() {
        double a = 1.0;
        return new Array2DRowRealMatrix(new double[][]{
                {a, 0.0, 0.0, 0.0, 0.0, 0.0},
                {0.0, a, 0.0, 0.0, 0.0, 0.0},
                {0.0, 0.0, 0.0, 0.0, a, 0.0},
                {0.0, 0.0, 0.0, 0.0, 0.0, a},
        });
    }

    private RealMatrix initR(double noisePosition, double noiseAcceleration) {
        double rp = Math.pow(noisePosition, 2);
        //double ra = Math.pow(noiseAcceleration, 2);
        double ra = Math.pow(10.0, 2);
        return MatrixUtils.createRealDiagonalMatrix(new double[]{rp, rp, ra, ra});
    }

    private RealMatrix initQ() {
        double a = 1.0;
        double s = 0.5 * Math.pow(dt, 2);
        double sa = Math.pow(0.001, 2);
        RealMatrix SA = MatrixUtils.createRealDiagonalMatrix(new double[]{sa, sa, sa, sa, sa, sa});
        RealMatrix G = new Array2DRowRealMatrix(new double[]{s, s, dt, dt, a, a});
        return G.multiply(G.transpose()).multiply(SA);
    }

    public double[] process(double[] acceleration, double[] gps, boolean locationChanged) {
        // Project the state ahead
        x = A.multiply(x);

        // Project the error covariance ahead
        P = A.multiply(P).multiply(A.transpose()).add(Q);

        if (locationChanged) {
            // Compute the Kalman Gain
            RealMatrix S = H.multiply(P).multiply(H.transpose()).add(R);
            RealMatrix SI = getInverse(S);
            RealMatrix K = (P.multiply(H.transpose())).multiply(SI);

            // Update the estimate via z
            double[] both = new double[]{gps[0], gps[1], acceleration[0], acceleration[1]};
            RealMatrix Z = new Array2DRowRealMatrix(both);
            RealMatrix y = Z.subtract(H.multiply(x));
            x = x.add(K.multiply(y));

            // Update the error covariance
            P = (I.subtract(K.multiply(H))).multiply(P);
        }
        return x.getColumn(0);

    }

    public double[] getX(){
        return x.getColumn(0);
    }

    private RealMatrix getInverse(RealMatrix matrix) {
        return new LUDecomposition(matrix).getSolver().getInverse();
    }

    public void setX(double[] position, double[] velocity, double[] acceleration) {
        x = new Array2DRowRealMatrix(new double[][]{{position[0], position[1], velocity[0], velocity[1], acceleration[0], acceleration[1]}}).transpose();
    }

}

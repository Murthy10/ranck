package com.murthy.ranck.sensors;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.murthy.ranck.R;
import com.murthy.ranck.data.ServerLogger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;


public class InertialMeasurementUnit {
    private final String TAG = this.getClass().getSimpleName();

    private AccelerationSensor mAcceleration;
    private GpsSensor mGps;
    private double[] mOldLocation;
    private KalmanFilter2D mKalmanFilter;
    private boolean mKalmanIsInit;
    private int mInterval; //Milliseconds

    private int mUpdateDelay;
    private int mUpdateCounter;

    private Consumer<double[]> mCallback;

    private ServerLogger mServerLogger;
    private SharedPreferences mPreferences;
    private Context mContext;

    public InertialMeasurementUnit(Context context, Consumer<double[]> callback, int interval, GpsSensor gpsSensor) {
        this.mCallback = callback;
        this.mServerLogger = new ServerLogger(context);
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.mContext = context;
        this.mInterval = interval;
        this.mGps = gpsSensor;
        init();
    }

    private void init() {
        int delay = 5000; //Milliseconds
        mUpdateDelay = 1000 / mInterval;
        mKalmanFilter = new KalmanFilter2D(mInterval / 1000.0); //Seconds
        mAcceleration = new AccelerationSensor(this.mContext);
        initTimer(delay);
    }


    private void process() {
        if (mGps.isReady()) {
            double[] location;
            boolean locationChanged = mGps.getLocationChanged();
            if (locationChanged) {
                location = mGps.getLocationXYZ();
                mOldLocation = location;
                mGps.locationChangedToggle();
            } else {
                location = mOldLocation;
            }
            if (location != null) {
                double[] acc = mAcceleration.getAccelerationRelativeToEarthXY();
                if (!mKalmanIsInit) initKalman(location, acc);
                double[] values = mKalmanFilter.process(acc, location, locationChanged);
                updateDisplay(values);
                log();
            }
        }
    }

    private void log() {
        if (isServerLogEnabled()) {
            logAccelerationToServer();
            logGpsToServer();
        }
    }

    private void logAccelerationToServer() {
        double[] acc = mAcceleration.getAccelerationXY();
        double[] accR = mAcceleration.getAccelerationRelativeToEarthXY();
        JSONObject jsonObjectAcc = new JSONObject();
        try {
            jsonObjectAcc.put("ax", acc[0]);
            jsonObjectAcc.put("ay", acc[1]);
            jsonObjectAcc.put("rx", accR[0]);
            jsonObjectAcc.put("ry", accR[1]);
            mServerLogger.logBuffer("acc", jsonObjectAcc);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void logGpsToServer() {
        double[] coordination = this.mGps.getLocationXYZ();
        double[] coordinationImu = this.mKalmanFilter.getX();
        JSONObject jsonObjectGps = new JSONObject();
        try {
            jsonObjectGps.put("x", coordination[0]);
            jsonObjectGps.put("y", coordination[1]);
            jsonObjectGps.put("kx", coordinationImu[0]);
            jsonObjectGps.put("ky", coordinationImu[1]);
            mServerLogger.logBuffer("mGps", jsonObjectGps);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateDisplay(double[] values) {
        if (mUpdateDelay == mUpdateCounter) {
            mCallback.accept(values);
            mUpdateCounter = 0;
        } else {
            mUpdateCounter++;
        }
    }

    private void initKalman(double[] locationInMeters, double[] acc) {
        double[] velocity = new double[]{0.0, 0.0};
        mKalmanFilter.setX(locationInMeters, velocity, acc);
        mKalmanIsInit = true;
    }

    private void initTimer(int delay) {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                process();
            }
        };
        timer.scheduleAtFixedRate(task, delay, mInterval);
    }

    private boolean isServerLogEnabled() {
        return mPreferences.getBoolean(this.mContext.getString(R.string.log_server_enable_key), false);
    }
}

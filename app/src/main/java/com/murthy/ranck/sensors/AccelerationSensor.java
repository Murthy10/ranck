package com.murthy.ranck.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.Arrays;

public class AccelerationSensor implements SensorEventListener {
    private final String TAG = this.getClass().getSimpleName();
    private SensorManager mSensorManager;

    private Sensor mAccelerationSensor;
    private Sensor mGravitySensor;
    private Sensor mMagneticFieldSensor;

    private float[] mAccelerationRelativeToEarth = new float[3];
    private final float[] mAccelerometerReading = new float[3];
    private final float[] mMagnetometerReading = new float[3];
    private final float[] mGravityReading = new float[3];


    public AccelerationSensor(Context context) {
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mAccelerationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);  //Sensor.TYPE_LINEAR_ACCELERATION
        //mAccelerationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);  //Sensor.TYPE_LINEAR_ACCELERATION
        mGravitySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        mMagneticFieldSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mSensorManager.registerListener(this, mGravitySensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mMagneticFieldSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mAccelerationSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        if (mGravityReading != null && mMagnetometerReading != null && event.sensor == mAccelerationSensor) {
            System.arraycopy(event.values, 0, mAccelerometerReading, 0, mAccelerometerReading.length);
            calculateAccelerationRelativeToEarth();
        } else if (event.sensor == mGravitySensor) {
            System.arraycopy(event.values, 0, mGravityReading, 0, mGravityReading.length);
        } else if (event.sensor == mMagneticFieldSensor) {
            System.arraycopy(event.values, 0, mMagnetometerReading, 0, mMagnetometerReading.length);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.i(TAG, "AccuracyChanged: " + String.valueOf(accuracy));
    }


    public double[] getAccelerationXY() {
        return new double[]{mAccelerometerReading[0], mAccelerometerReading[1]};
    }


    public double[] getAccelerationRelativeToEarthXY() {
        return new double[]{mAccelerationRelativeToEarth[0], mAccelerationRelativeToEarth[1]};
    }


    // Change the device relative acceleration values to earth relative values
    // X axis -> East
    // Y axis -> North Pole
    // Z axis -> Sky
    //https://stackoverflow.com/questions/5464847/transforming-accelerometers-data-from-devices-coordinates-to-real-world-coordi
    //https://thestatemachine.wordpress.com/2016/07/30/device-motion-in-earth-coordinates/
    private void calculateAccelerationRelativeToEarth() {
        float[] R = new float[16], I = null, earthAcc = new float[16], transposeR = new float[16];
        Log.i("Acceleration", Arrays.toString(mAccelerometerReading));
        float[] deviceRelativeAcceleration = new float[4];
        deviceRelativeAcceleration[0] = mAccelerometerReading[0];
        deviceRelativeAcceleration[1] = mAccelerometerReading[1];
        deviceRelativeAcceleration[2] = mAccelerometerReading[2];
        deviceRelativeAcceleration[3] = 0;


        SensorManager.getRotationMatrix(R, null, mGravityReading, mMagnetometerReading);
        android.opengl.Matrix.transposeM(transposeR, 0, R, 0);
        android.opengl.Matrix.multiplyMV(earthAcc, 0, transposeR, 0, deviceRelativeAcceleration, 0);
        mAccelerationRelativeToEarth[0] = earthAcc[0];
        mAccelerationRelativeToEarth[1] = earthAcc[1];
        mAccelerationRelativeToEarth[2] = earthAcc[2];

        //Log.i("Acceleration", Arrays.toString(mAccelerometerReading));
        //Log.i("Gravity", Arrays.toString(mGravityReading));
        //Log.i("Magnetometer", Arrays.toString(mMagnetometerReading));
        //Log.i("Rotation", Arrays.toString(R));
        Log.i("RelativeToEarth", Arrays.toString(mAccelerationRelativeToEarth));
    }

}

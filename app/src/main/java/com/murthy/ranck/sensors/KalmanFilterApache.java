package com.murthy.ranck.sensors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.filter.DefaultMeasurementModel;
import org.apache.commons.math3.filter.DefaultProcessModel;
import org.apache.commons.math3.filter.KalmanFilter;
import org.apache.commons.math3.filter.MeasurementModel;
import org.apache.commons.math3.filter.ProcessModel;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class KalmanFilterApache {
    private final String TAG = this.getClass().getSimpleName();
    public RealVector x;
    public RealMatrix P;
    public RealMatrix A;
    public RealMatrix H;
    public RealMatrix R;
    public RealMatrix Q;
    public RealMatrix I;
    public RealMatrix B;
    private double dt; // Time Step between Filter Steps
    private KalmanFilter filter;

    public KalmanFilterApache(double dt) {
        init(dt);
    }

    private void init(double dt) {
        //[ x-Position, y-Position, z-Position, x-Velocity, y-Velocity, z-Velocity, x-AccelerationSensor, y-AccelerationSensor, z-AccelerationSensor]
        this.dt = dt;
        x = new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0});

        //uncertainty
        double noisePosition = 10.0;
        double noiseVelocity = 1.0;
        double noiseAcceleration = 0.2;
        P = initP(noisePosition, noiseVelocity, noiseAcceleration);

        //Dynamic Matrix
        A = initA();

        // Measurement Matrix
        H = initH();

        // AccelerationSensor Measurement Noise Covariance
        R = initR(noisePosition, noiseAcceleration);

        // Process Noise Covariance Matrix
        Q = initQ();

        // Identity Matrix
        I = MatrixUtils.createRealIdentityMatrix(6);

        B = new Array2DRowRealMatrix(new double[][]{{0.0, 0.0, 0.0, 0.0, 0.0, 0.0}}).transpose();

        ProcessModel pm = new DefaultProcessModel(A, B, Q, x, P);
        MeasurementModel mm = new DefaultMeasurementModel(H, R);
        filter = new KalmanFilter(pm, mm);
    }

    private RealMatrix initP(double noisePosition, double noiseVelocity, double noiseAcceleration) {
        return MatrixUtils.createRealDiagonalMatrix(new double[]{noisePosition, noisePosition, noiseVelocity, noiseVelocity, noiseAcceleration, noiseAcceleration});
    }

    private RealMatrix initA() {
        double a = 1.0;
        double s = 0.5 * Math.pow(dt, 2);
        return new Array2DRowRealMatrix(new double[][]{
                {a, 0.0, dt, 0.0, s, 0.0},
                {0.0, a, 0.0, dt, 0.0, s},
                {0.0, 0.0, a, 0.0, dt, 0.0},
                {0.0, 0.0, 0.0, a, 0.0, dt},
                {0.0, 0.0, 0.0, 0.0, a, 0.0},
                {0.0, 0.0, 0.0, 0.0, 0.0, a},
        });
    }


    private RealMatrix initH() {
        double a = 1.0;
        return new Array2DRowRealMatrix(new double[][]{
                {a, 0.0, 0.0, 0.0, 0.0, 0.0},
                {0.0, a, 0.0, 0.0, 0.0, 0.0},
                {0.0, 0.0, 0.0, 0.0, a, 0.0},
                {0.0, 0.0, 0.0, 0.0, 0.0, a},
        });
    }

    private RealMatrix initR(double noisePosition, double noiseAcceleration) {
        double rp = Math.pow(noisePosition, 2);
        double ra = Math.pow(noiseAcceleration, 2);
        return new Array2DRowRealMatrix(new double[]{rp, rp, ra, ra});
    }

    private RealMatrix initQ() {
        double a = 1.0;
        double s = 0.5 * Math.pow(dt, 2);
        double sa = Math.pow(0.001, 2);
        RealMatrix SA = MatrixUtils.createRealDiagonalMatrix(new double[]{sa, sa, sa, sa, sa, sa});
        RealMatrix G = new Array2DRowRealMatrix(new double[]{s, s, dt, dt, a, a});
        return G.multiply(G.transpose()).multiply(SA);
    }

    public double[] process(double[] acceleration, double[] gps, boolean locationChanged) {
        filter.predict();
        double[] both = ArrayUtils.addAll(gps, acceleration);
        RealVector z = new ArrayRealVector(both);
        filter.correct(z);
        return filter.getStateEstimation();
    }

}

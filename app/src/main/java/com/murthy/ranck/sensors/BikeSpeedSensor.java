package com.murthy.ranck.sensors;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeCadencePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc;
import com.dsi.ant.plugins.antplus.pcc.MultiDeviceSearch;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceType;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;
import com.dsi.ant.plugins.antplus.pccbase.MultiDeviceSearch.MultiDeviceSearchResult;
import com.murthy.ranck.R;

import java.math.BigDecimal;
import java.util.EnumSet;

public class BikeSpeedSensor {
    private final String TAG = this.getClass().getSimpleName();
    private SharedPreferences preferences;
    private Context context;
    private MultiDeviceSearchResult mMultiDeviceSearchResult;
    private AntPlusBikeSpeedDistancePcc bsdPcc = null;
    private PccReleaseHandle<AntPlusBikeSpeedDistancePcc> bsdReleaseHandle = null;
    private PccReleaseHandle<AntPlusBikeCadencePcc> bcReleaseHandle = null;
    private AntPluginPcc.IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc> mResultReceiver;
    private AntPluginPcc.IDeviceStateChangeReceiver mDeviceStateChangeReceiver;

    private double mSpeed;
    private double distance;

    public BikeSpeedSensor(Context context) {
        this.context = context;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        initReceiver();
        searchForDevices();
    }


    private void resetPcc() {
        //Release the old access if it exists
        if (bsdReleaseHandle != null) {
            bsdReleaseHandle.close();
        }
        if (bcReleaseHandle != null) {
            bcReleaseHandle.close();
        }
    }

    protected void destroy() {
        bsdReleaseHandle.close();
        if (bcReleaseHandle != null) {
            bcReleaseHandle.close();
        }
    }


    private void searchForDevices() {
        EnumSet<DeviceType> deviceTypes = EnumSet.of(DeviceType.BIKE_SPD);
        MultiDeviceSearch.SearchCallbacks searchCallbacks = new MultiDeviceSearch.SearchCallbacks() {
            @Override
            public void onSearchStarted(MultiDeviceSearch.RssiSupport rssiSupport) {
                Log.i(TAG, "Search stared");
            }

            @Override
            public void onDeviceFound(MultiDeviceSearchResult multiDeviceSearchResult) {
                mMultiDeviceSearchResult = multiDeviceSearchResult;
                Log.i(TAG, "Found device: " + multiDeviceSearchResult.getDeviceDisplayName());
                Log.i(TAG, "Device type: " + multiDeviceSearchResult.getAntDeviceType());
                Log.i(TAG, "Device number: " + multiDeviceSearchResult.getAntDeviceNumber());
                requestDeviceAccess(mMultiDeviceSearchResult);
            }

            @Override
            public void onSearchStopped(RequestAccessResult requestAccessResult) {
                Log.i(TAG, "Search stopped");
            }
        };
        MultiDeviceSearch multiDeviceSearch = new MultiDeviceSearch(context, deviceTypes, searchCallbacks);
    }

    private void requestDeviceAccess(MultiDeviceSearchResult multiDeviceSearchResult) {
        boolean isBSC = multiDeviceSearchResult.getAntDeviceType().equals(DeviceType.BIKE_SPDCAD);
        AntPlusBikeSpeedDistancePcc.requestAccess(context, multiDeviceSearchResult.getAntDeviceNumber(), 0, isBSC, mResultReceiver, mDeviceStateChangeReceiver);
    }

    private void subscribeToEvents() {
        // 2.095m circumference = an average 700cx23mm road tire
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String wheelCircumference = preferences.getString(context.getString(R.string.wheel_circumference_key), "2.095");
        float wheelCircumferenceFloat = Float.parseFloat(wheelCircumference);
        bsdPcc.subscribeCalculatedSpeedEvent(new AntPlusBikeSpeedDistancePcc.CalculatedSpeedReceiver(new BigDecimal(wheelCircumferenceFloat)) {
            @Override
            public void onNewCalculatedSpeed(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final BigDecimal calculatedSpeed) {
                //Log.i(TAG, "Calculated Speed: " + String.valueOf(calculatedSpeed));
                setSpeed(calculatedSpeed.doubleValue());
            }
        });

        bsdPcc.subscribeCalculatedAccumulatedDistanceEvent(new AntPlusBikeSpeedDistancePcc.CalculatedAccumulatedDistanceReceiver(new BigDecimal(2.095)) { // 2.095m circumference = an average // 700cx23mm road tire
            @Override
            public void onNewCalculatedAccumulatedDistance(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final BigDecimal calculatedAccumulatedDistance) {
                //Log.i(TAG, "Calculated Accumulated Distance: " + String.valueOf(calculatedAccumulatedDistance.setScale(3, RoundingMode.HALF_UP)));
                setDistance(calculatedAccumulatedDistance.doubleValue());
            }
        });

        bsdPcc.subscribeRawSpeedAndDistanceDataEvent(new AntPlusBikeSpeedDistancePcc.IRawSpeedAndDistanceDataReceiver() {
            @Override
            public void onNewRawSpeedAndDistanceData(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final BigDecimal timestampOfLastEvent, final long cumulativeRevolutions) {
                //Log.i(TAG, "Cumulative revolutions: " + String.valueOf(cumulativeRevolutions));
            }
        });
        bsdPcc.subscribeMotionAndSpeedDataEvent(new AntPlusBikeSpeedDistancePcc.IMotionAndSpeedDataReceiver() {
            @Override
            public void onNewMotionAndSpeedData(final long estTimestamp, EnumSet<EventFlag> eventFlags, final boolean isStopped) {
                //Log.i(TAG, "Is Stopped: " + String.valueOf(isStopped));
            }
        });
    }

    private void initReceiver() {
        mResultReceiver = new AntPluginPcc.IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc>() {
            @Override
            public void onResultReceived(AntPlusBikeSpeedDistancePcc result, RequestAccessResult resultCode, DeviceState initialDeviceState) {
                switch (resultCode) {
                    case SUCCESS:
                        bsdPcc = result;
                        Log.i(TAG, result.getDeviceName() + ": " + initialDeviceState);
                        subscribeToEvents();
                        break;
                    case CHANNEL_NOT_AVAILABLE:
                        Log.e(TAG, "Channel Not Available");
                        break;
                    case ADAPTER_NOT_DETECTED:
                        Log.e(TAG, "ANT Adapter Not Available. Built-in ANT hardware or external adapter required.");
                        break;
                    case BAD_PARAMS:
                        // Note: Since we compose all the params ourself, we should
                        // never see this result
                        Log.e(TAG, "Bad request parameters.");
                        break;
                    case OTHER_FAILURE:
                        Log.e(TAG, "RequestAccess failed. See logcat for details.");
                        break;
                    case DEPENDENCY_NOT_INSTALLED:
                        Log.e(TAG, "Missing Dependency");
                        break;
                    case USER_CANCELLED:
                        break;
                    case UNRECOGNIZED:
                        Log.e(TAG, "Failed: UNRECOGNIZED. PluginLib Upgrade Required?");
                        break;
                    default:
                        Log.e(TAG, "Unrecognized result: " + resultCode);
                        break;
                }
            }
        };

        // Receives state changes and shows it on the status display line
        monitorDeviceStateChanges();
    }

    private void monitorDeviceStateChanges(){
        mDeviceStateChangeReceiver = new AntPluginPcc.IDeviceStateChangeReceiver() {
            @Override
            public void onDeviceStateChange(final DeviceState newDeviceState) {
                Log.i(TAG, bsdPcc.getDeviceName() + ": " + newDeviceState);
                if (newDeviceState == DeviceState.DEAD) bsdPcc = null;
            }
        };
    }

    public double getSpeed() {
        return this.mSpeed;
    }

    public double getDistance() {
        return this.distance;
    }

    private void setSpeed(double mSpeed) {
        this.mSpeed = mSpeed;
    }

    private void setDistance(double distance) {
        this.distance = distance;
    }
}



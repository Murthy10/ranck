package com.murthy.ranck.sensors;

import android.content.Context;
import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.murthy.ranck.utils.LocationConverter;


public class GpsSensor implements LocationListener {
    private final String TAG = this.getClass().getSimpleName();
    private LocationManager mLocationManager;
    private GnssStatus mGnssStatus;
    private boolean mReady;
    private boolean mLocationChanged;
    private double mAccuracy; //Value indicates 68% confident that measurement is this radius

    private Location location;

    public GpsSensor(Context context) {
        String locationProvider = LocationManager.GPS_PROVIDER;
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        mLocationManager.requestLocationUpdates(locationProvider, 0, 0, this);
        mLocationChanged = false;
        mReady = false;
        mAccuracy = 100.;
        addGnssStatusListener();
    }

    @Override
    public void onLocationChanged(Location location) {
        mReady = true;
        setLocation(location);
        setAccuracy(location.getAccuracy());
        locationChangedToggle();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i(TAG, "onStatusChanged: " + String.valueOf(status));
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.i(TAG, "onProviderEnabled: " + provider);

    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.i(TAG, "onProviderEnabled: " + provider);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public double[] getLocationXYZ() {
        return new double[]{location.getLongitude(), location.getLatitude(), location.getAltitude()};
    }

    public boolean getLocationChanged() {
        return mLocationChanged;
    }

    public void locationChangedToggle() {
        mLocationChanged = !mLocationChanged;
    }

    public boolean isReady() {
        return mReady;
    }

    private void addGnssStatusListener() {
        GnssStatus.Callback gnssStatusListener = new GnssStatus.Callback() {
            @Override
            public void onSatelliteStatusChanged(GnssStatus status) {
                mGnssStatus = status;
            }
        };
        mLocationManager.registerGnssStatusCallback(gnssStatusListener);
    }

    public double getCurrentSpeed() {
        return this.getLocation().getSpeed();
    }

    private void setAccuracy(double accuracy) {
        mAccuracy = accuracy;
    }

    public double getAccuracy() {
        return mAccuracy;
    }
}

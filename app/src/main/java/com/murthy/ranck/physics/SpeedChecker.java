package com.murthy.ranck.physics;

import android.content.Context;

import static com.murthy.ranck.physics.PhysicalConstants.G;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

public class SpeedChecker {
    private static double ORANGE_AREA = 0.7; //upper 30 Percent
    private Driver driver;

    public SpeedChecker() {
    }

    public SpeedChecker(Context context) {
        driver = new Driver(context);
    }

    public boolean check(double speed, double radius, double friction, double slope) {
        return frictionForce(driver.getMass(), slope, friction) >= centrifugalForce(driver.getMass(), speed, radius);
    }

    public double centrifugalForce(double mass, double speed, double radius) {
        return (mass * pow(speed, 2.0)) / radius;
    }

    public double frictionForce(double mass, double slope, double friction) {
        return friction * ((mass * G) / cos(slope));
    }

    public double maxSpeed(double radius, double friction, double slope) {
        double counter = (radius * G) * (sin(slope) + friction * cos(slope));
        double denominator = cos(slope) + friction * sin(slope);
        return sqrt((counter / denominator));
    }

    // -1 reed, 0 orange, 1 green
    public double status(double currentSpeed, double speedLimit) {
        double changeSpeedLimit = driver.getSkill() * speedLimit;
        if (currentSpeed >= changeSpeedLimit) {
            return -1.;
        } else if (currentSpeed >= changeSpeedLimit * ORANGE_AREA) {
            return 0.;
        } else {
            return 1.;
        }
    }
}

package com.murthy.ranck.physics;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.murthy.ranck.R;

public class Driver {
    private final String TAG = this.getClass().getSimpleName();

    private SharedPreferences preferences;
    private Context context;

    public Driver(Context context) {
        this.context = context;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public double getMass() {
        return preferences.getInt(context.getString(R.string.weight_of_driver_key), 70);
    }

    public double getSkill() {
        return (10 + preferences.getInt(context.getString(R.string.sensitivity_key), 50)) / 100.0;
    }
}

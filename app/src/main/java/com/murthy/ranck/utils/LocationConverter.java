package com.murthy.ranck.utils;


import com.vividsolutions.jts.geom.Coordinate;

import org.osgeo.proj4j.CRSFactory;
import org.osgeo.proj4j.CoordinateReferenceSystem;
import org.osgeo.proj4j.CoordinateTransform;
import org.osgeo.proj4j.CoordinateTransformFactory;
import org.osgeo.proj4j.ProjCoordinate;

import static java.lang.Math.PI;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

//https://gist.githubusercontent.com/klucar/1536194/raw/49053140bb9df5956c5e9d8782e1d987cd2ae4c0/gistfile1.java
public class LocationConverter {

    private final double radius = 6378137;
    private final double eccentricity = 8.1819190842622e-2;

    private final double asq = pow(radius, 2);
    private final double esq = pow(eccentricity, 2);


    private CoordinateTransform llaToWebMercatorTransform;
    private CoordinateTransform webMercatorToLlaTransform;

    public LocationConverter() {
        String llaEPSG = "EPSG:4326";
        String webMercatorEPSG = "EPSG:3857";
        CRSFactory csFactory = new CRSFactory();
        CoordinateReferenceSystem lla = csFactory.createFromName(llaEPSG);
        CoordinateReferenceSystem webMercator = csFactory.createFromName(webMercatorEPSG);
        CoordinateTransformFactory coordinateTransformFactory = new CoordinateTransformFactory();
        //ecefToLlaTransform = coordinateTransformFactory.createTransform(ECEF, LLA);
        llaToWebMercatorTransform = coordinateTransformFactory.createTransform(lla, webMercator);
        webMercatorToLlaTransform = coordinateTransformFactory.createTransform(webMercator, lla);
    }

    public double[] ecefToLla(double[] ecef) {
        double x = ecef[0];
        double y = ecef[1];
        double z = ecef[2];

        double b = sqrt(asq * (1 - esq));
        double bsq = pow(b, 2);
        double ep = sqrt((asq - bsq) / bsq);
        double p = sqrt(pow(x, 2) + pow(y, 2));
        double th = atan2(radius * z, b * p);

        double lon = atan2(y, x);
        double lat = atan2((z + pow(ep, 2) * b * pow(sin(th), 3)), (p - esq * radius * pow(cos(th), 3)));
        double N = radius / (sqrt(1 - esq * pow(sin(lat), 2)));
        double alt = p / cos(lat) - N;

        // mod lat to 0-2pi
        lon = lon % (2 * PI);

        // correction for altitude near poles left out.
        return new double[]{Math.toDegrees(lat), Math.toDegrees(lon), alt};
    }


    public double[] llaToEcef(double[] lla) {
        double lat = Math.toRadians(lla[0]);
        double lon = Math.toRadians(lla[1]);
        double alt = lla[2];

        double N = radius / sqrt(1 - esq * pow(sin(lat), 2));

        double x = (N + alt) * cos(lat) * cos(lon);
        double y = (N + alt) * cos(lat) * sin(lon);
        double z = ((1 - esq) * N + alt) * sin(lat);

        return new double[]{x, y, z};
    }


    public double[] llaToWebMercator(double[] lla) {
        double lon = lla[0];
        double lat = lla[1];
        ProjCoordinate sourcePoint = new ProjCoordinate(lon, lat);
        ProjCoordinate destinationPoint = new ProjCoordinate();
        llaToWebMercatorTransform.transform(sourcePoint, destinationPoint);
        return new double[]{destinationPoint.x, destinationPoint.y};
    }

    public double[] webMercatorToLla(double[] webMercator) {
        double x = webMercator[0];
        double y = webMercator[1];
        ProjCoordinate sourcePoint = new ProjCoordinate(x, y);
        ProjCoordinate destinationPoint = new ProjCoordinate();
        webMercatorToLlaTransform.transform(sourcePoint, destinationPoint);
        return new double[]{destinationPoint.x, destinationPoint.y};
    }
}

package com.murthy.ranck.utils;


import android.util.Log;

import com.murthy.ranck.tracks.Track;
import com.vividsolutions.jts.geom.Coordinate;

public class LocationEstimator {
    private final String TAG = this.getClass().getSimpleName();
    private Track mTrack;
    private double mDeltaT;
    private double mDrivenDistance;
    private double mCurrentDistance;
    private int mCoordinateIndex;
    private boolean mIsStarted;

    public LocationEstimator(Track track, int interval) {
        mTrack = track;
        mDeltaT = interval / 1000.;
        mDrivenDistance = 0.;
        mCurrentDistance = 0;
        mIsStarted = false;
    }

    public void startEstimation(Coordinate lastKnowLocation) {
        startEstimation(new double[]{lastKnowLocation.x, lastKnowLocation.y});
    }


    public void startEstimation(double[] lastKnowLocation) {
        mIsStarted = true;
        Coordinate nearestCoordinate = mTrack.getNearestCoordinate(lastKnowLocation[0], lastKnowLocation[1]);
        mCoordinateIndex = mTrack.getIndexOfCoordinate(nearestCoordinate);
    }

    public double[] estimateLocation(double currentSpeed, double[] lastKnowLocation) {
        if (!mIsStarted) startEstimation(lastKnowLocation);
        Coordinate estimatedCoordinate = estimateLocation(currentSpeed);
        return new double[]{estimatedCoordinate.x, estimatedCoordinate.y};
    }

    public Coordinate estimateLocation(double currentSpeed) {
        Coordinate currentCoordinate = mTrack.getCoordinateByIndex(mCoordinateIndex);
        double distance = calculateDistance(currentSpeed);
        while (distance > mCurrentDistance && mCoordinateIndex < mTrack.numberOfPoints()) {
            Coordinate nextCoordinate = mTrack.getCoordinateByIndex(++mCoordinateIndex);
            if (nextCoordinate == currentCoordinate) break;
            mCurrentDistance += currentCoordinate.distance(nextCoordinate);
            currentCoordinate = nextCoordinate;
        }
        /*
        Log.i(TAG, "Distance: " + mDrivenDistance);
        Log.i(TAG, "Current Distance: " + mCurrentDistance);
        Log.i(TAG, "Index: " + mCoordinateIndex);
        */
        return mTrack.getCoordinateByIndex(mCoordinateIndex - 1);
    }

    private double calculateDistance(double currentSpeed) {
        mDrivenDistance += mDeltaT * currentSpeed;
        return mDrivenDistance;
    }


    public void resetEstimation() {
        mDrivenDistance = 0;
        mCurrentDistance = 0;
        mIsStarted = false;
    }
}

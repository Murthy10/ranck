package com.murthy.ranck.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.murthy.ranck.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ServerLogger {
    private final String TAG = this.getClass().getSimpleName();
    private RequestQueue mRequestQueue;
    private Context context;
    private int bufferSize;
    private String uuid;
    private Map<String, List<JSONObject>> mapBuffer;
    private SharedPreferences preferences;


    public ServerLogger(Context context) {
        this.uuid = UUID.randomUUID().toString();
        this.bufferSize = 100;
        this.context = context;
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.mRequestQueue = getRequestQueue();
        this.mapBuffer = new HashMap<>();
    }

    public void logBuffer(String type, JSONObject entry) {
        List<JSONObject> data = mapBuffer.get(type);
        if (data == null) {
            mapBuffer.put(type, new ArrayList<JSONObject>());
            data = mapBuffer.get(type);
        }

        data.add(entry);
        if (data.size() > bufferSize) {
            this.log(type, data);
            mapBuffer.remove(type);
        }
    }

    public void log(String type, List<JSONObject> data) {
        String url = getLogServerAddress();
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (JSONObject d : data) {
                JSONObject jsonEntry = new JSONObject();
                jsonEntry.put("value", d);
                jsonArray.put(jsonEntry);
            }
            jsonObject.put("uuid", uuid);
            jsonObject.put("type", type);
            jsonObject.put("entries", jsonArray);
            this.jsonRequest(url, jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void jsonRequest(String url, JSONObject jsonObject) throws JSONException {
        Log.i(TAG, "Request Url: " + url);
        Log.i(TAG, "JsonObject" + jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        JSONObject badResponse = new JSONObject();
                        try {
                            badResponse.put("onErrorResponse", error.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i(TAG, error.toString());
                    }
                });
        mRequestQueue.add(jsonObjectRequest);
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return mRequestQueue;
    }

    private String getLogServerAddress() {
        return preferences.getString(context.getString(R.string.log_server_key), context.getString(R.string.log_server_default));
    }
}

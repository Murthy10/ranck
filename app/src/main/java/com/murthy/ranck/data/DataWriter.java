package com.murthy.ranck.data;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class DataWriter {
    private final String TAG = this.getClass().getSimpleName();
    private CSVWriter writer;

    public DataWriter() {
        String fileName = "ranck.csv";
        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/ranck");
            if (myDir.exists())myDir.delete();
            myDir.mkdirs();
            File file = new File(myDir, fileName);
            if (file.exists()) file.delete();
            file.createNewFile();
            writer = new CSVWriter(new FileWriter(file));
            writer.writeNext(new String[]{"kPosX", "kPosY", "kPosZ", "kVelX", "kVelY", "kVelZ", "kAccX", "kAccY", "kAccZ", "PosX", "PosY","PosZ", "AccX", "AccY", "AccZ"});
        } catch (IOException e) {
            e.printStackTrace();
            Log.i(TAG, e.toString());
        }
    }


    public void wirteRecord(String[] record) {
        writer.writeNext(record);
    }


}

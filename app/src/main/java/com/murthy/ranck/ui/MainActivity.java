package com.murthy.ranck.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.murthy.ranck.R;
import com.murthy.ranck.ui.preferences.SettingsActivity;
import com.murthy.ranck.services.RanckReceiver;
import com.murthy.ranck.services.RanckService;

import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, EasyPermissions.PermissionCallbacks {

    private final String TAG = this.getClass().getSimpleName();
    private static final int RC_PERMISSIONS = 124;
    private static final String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

    private Intent ranckIntent;
    private RanckReceiver ranckReceiver;

    private TextView textViewSpeed;
    private ImageView imageViewArrow;
    private ConstraintLayout layoutMain;
    private ProgressBar loadBar;
    private Vibrator vibrator;

    private boolean screenLocked;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        init();
        requestPermissions();
    }

    @Override
    protected void onStart() {
        super.onStart();
        screenLocked = false;
    }


    @Override
    protected void onStop() {
        super.onStop();
        screenLocked = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(ranckIntent);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(ranckReceiver);
    }


    private void init() {
        textViewSpeed = (TextView) findViewById(R.id.textViewSpeed);
        imageViewArrow = (ImageView) findViewById(R.id.imageViewArrow);
        layoutMain = (ConstraintLayout) findViewById(R.id.mainContent);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        loadBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    private void manageRanckService() {
        ranckIntent = new Intent(this, RanckService.class);
        IntentFilter imuFilter = new IntentFilter(RanckService.RANCK_SERVICE);
        ranckReceiver = new RanckReceiver();
        ranckReceiver.setCallback(this::getRanckValues);
        LocalBroadcastManager.getInstance(this).registerReceiver(ranckReceiver, imuFilter);
        startService(ranckIntent);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean hasPermissions() {
        return EasyPermissions.hasPermissions(this, PERMISSIONS);
    }

    @AfterPermissionGranted(RC_PERMISSIONS)
    public void requestPermissions() {
        if (hasPermissions()) {
            manageRanckService();
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.location_rationale), RC_PERMISSIONS, PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            Toast.makeText(this, String.valueOf(hasPermissions()), Toast.LENGTH_LONG).show();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getRanckValues(double[] values) {
        loadBar.setVisibility(View.GONE);
        double currentSpeed = values[0];
        double direction = values[1];
        double radius = values[2];
        double status = values[3];
        if (screenLocked) {
            vibrate(status);
        } else {
            setSpeedText(currentSpeed);
            setBackgroundColor(status);
            setDirectionImage(direction, radius);
        }
    }

    // -1 reed, 0 orange, 1 green
    private void setBackgroundColor(double status) {
        switch ((int) status) {
            case -1:
                layoutMain.setBackgroundColor(getColor(R.color.danger));
                break;
            case 0:
                layoutMain.setBackgroundColor(getColor(R.color.warning));
                break;
            default:
                layoutMain.setBackgroundColor(getColor(R.color.success));
                break;
        }
    }

    //0 = forward, 1 = left, -1 = right
    private void setDirectionImage(double direction, double radius) {
        //Log.i(TAG, "Direction: " + direction + " Radius: " + radius);
        if (direction == 0.0) {
            imageViewArrow.setImageResource(R.drawable.ic_arrow_upward);
        } else if (direction < 0.0) {
            if (radius > 30.0) {
                imageViewArrow.setImageResource(R.drawable.ic_arrow_right_small);
            } else {
                imageViewArrow.setImageResource(R.drawable.ic_arrow_right);
            }
        } else {
            if (radius > 30.0) {
                imageViewArrow.setImageResource(R.drawable.ic_arrow_left_small);
            } else {
                imageViewArrow.setImageResource(R.drawable.ic_arrow_left);
            }
        }
    }

    private void setSpeedText(double currentSpeed) {
        String speedText = String.format("%1$,.1f km/h", currentSpeed);
        textViewSpeed.setText(speedText);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void vibrate(double status) {
        long[] mVibratePattern = new long[]{0, 200, 0};
        if (status <= 0.0 && vibrator != null && vibrator.hasVibrator()) {
            // -1 : Do not repeat this pattern
            // pass 0 if you want to repeat this pattern from 0th index
            vibrator.vibrate(mVibratePattern, -1);
            //vibrator.vibrate(VibrationEffect.createWaveform(new long[]{50, 200}, VibrationEffect.DEFAULT_AMPLITUDE));
        }
    }
}

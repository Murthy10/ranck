package com.murthy.ranck.ui.preferences;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.murthy.ranck.R;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = "PreferenceFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        setWeightSummary(getString(R.string.weight_of_driver_key));
        setWheelSummary(getString(R.string.wheel_circumference_key));
        setLogServer(getString(R.string.log_server_key));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        // remove dividers
        ListView lv = (ListView) view.getRootView().findViewById(android.R.id.list);
        if(lv != null){
            lv.setDivider(new ColorDrawable(Color.TRANSPARENT));
            lv.setDividerHeight(0);
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.v(TAG, "Shared Preferences changed: " + key);

        switch (key) {
            case "weight_of_driver_key":
                setWeightSummary(key);
                break;
            case "wheel_circumference_key":
                setWheelSummary(key);
                break;
            case "log_server_key":
                setLogServer(key);
                break;
        }
    }

    private void setLogServer(String key) {
        Preference pref = findPreference(key);
        if (pref instanceof EditTextPreference) {
            EditTextPreference editTextPreference = (EditTextPreference) pref;
            pref.setSummary(editTextPreference.getText());
        }
    }

    private void setWeightSummary(String key) {
        Preference preference = findPreference(key);
        if (preference instanceof NumberPickerPreference) {
            NumberPickerPreference listPref = (NumberPickerPreference) preference;
            String summary = String.valueOf(listPref.getValue()) + " kg";
            preference.setSummary(summary);
        }
    }

    private void setWheelSummary(String key) {
        Preference pref = findPreference(key);
        if (pref instanceof EditTextPreference) {
            EditTextPreference editTextPreference = (EditTextPreference) pref;
            pref.setSummary(editTextPreference.getText() + " m");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }
}
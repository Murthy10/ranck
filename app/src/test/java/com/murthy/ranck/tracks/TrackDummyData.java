package com.murthy.ranck.tracks;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;


public class TrackDummyData {

    private Track track;
    private GeometryFactory geometryFactory;
    private LineString lineString;
    private double[] radiuses;
    private double[] directions;

    public TrackDummyData(){
        geometryFactory = new GeometryFactory();
        lineString = getDummyLineString();
        radiuses = getDummyRadiuses();
        directions = getDummyDirections();
        track = new Track(lineString, radiuses, directions);
    }

    public Track getTrack(){
        return track;
    }

    private LineString getDummyLineString() {
        Coordinate[] coordinates = new Coordinate[10];
        for (int i = 0; i < 10; i++) {
            coordinates[i] = new Coordinate(i, 0);
        }
        return geometryFactory.createLineString(coordinates);
    }

    private double[] getDummyRadiuses() {
        double[] radiuses = new double[10];
        for (int i = 0; i < 10; i++) {
            radiuses[i] = (double) i;
        }
        return radiuses;
    }

    private double[] getDummyDirections() {
        double[] directions = new double[10];
        for (int i = 0; i < 10; i++) {
            directions[i] = -1.;
        }
        return directions;
    }

}

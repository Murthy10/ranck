package com.murthy.ranck.tracks;

import com.vividsolutions.jts.geom.Coordinate;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class TrackTest {
    private Track track;

    @Before
    public void setUp() {
        TrackDummyData trackDummyData = new TrackDummyData();
        this.track = trackDummyData.getTrack();
    }

    @Test
    public void getNearestCoordinateTest() {
        Coordinate coordinate = new Coordinate(2, 1, 0);
        Coordinate result = new Coordinate(2, 0, 0);
        Coordinate nearestCoordinate = track.getNearestCoordinate(coordinate);
        assertTrue(nearestCoordinate.equals2D(result));
    }

    @Test
    public void notNearestCoordinateTest() {
        Coordinate coordinate = new Coordinate(5, 1, 0);
        Coordinate result = new Coordinate(2, 0, 0);
        Coordinate nearestCoordinate = track.getNearestCoordinate(coordinate);
        assertFalse(nearestCoordinate.equals2D(result));
    }

    @Test
    public void getNextCurveRadiusTest() {
        Coordinate coordinate = new Coordinate(1, 0, 0);
        Curve curve = track.getNextCurve(coordinate);
        assertTrue(curve.getRadius() == 2.0);
    }

    @Test
    public void getNextCurveRadiusLastTest() {
        Coordinate coordinate = new Coordinate(11, 0, 0);
        Curve curve = track.getNextCurve(coordinate);
        assertTrue(curve.getRadius() == 9.);
    }

    @Test
    public void getNextCurveAwayTest() {
        Coordinate coordinate = new Coordinate(100, 100);
        Curve curve = track.getNextCurve(coordinate);
        assertTrue(curve.getRadius() == 9.0);
    }

    @Test
    public void getNearestCoordinateMultipleTimesTest() {
        Coordinate coordinate = new Coordinate(2, 1, 0);
        Coordinate result = new Coordinate(2, 0, 0);
        for (int i = 0; i < 100; i++) {
            Coordinate nearestCoordinate = track.getNearestCoordinate(coordinate);
            assertTrue(nearestCoordinate.equals2D(result));
        }
    }


    @Test
    public void getIndexOfCoordinateTest() {
        Coordinate coordinate = new Coordinate(1, 0, 0);
        int index = track.getIndexOfCoordinate(coordinate);
        assertTrue(index == 1);
    }

    @Test
    public void getIndexOfCoordinateAllTest() {
        for (int i = 0; i < track.numberOfPoints(); i++) {
            int index = track.getIndexOfCoordinate(track.getCoordinateByIndex(i));
            assertTrue(index == i);
        }
    }

    @Test
    public void getIndexOfCoordinateOutTest() {
        Coordinate coordinate = new Coordinate(1, 1, 0);
        int index = track.getIndexOfCoordinate(coordinate);
        assertTrue(index == -1);
    }


}



package com.murthy.ranck.tracks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class TrackNextDistanceTest {
    private Track track;

    @Before
    public void setUp() {
        TrackDummyData trackDummyData = new TrackDummyData();
        this.track = trackDummyData.getTrack();
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {0, 1.0}, {9, 0.}, {5, 1.0}
        });
    }

    @Parameterized.Parameter
    public int index;

    @Parameterized.Parameter(1)
    public double expectedDistance;

    @Test
    public void distanceToNextCurveTest() {
        double distance = track.distanceToNextCurve(index);
        assertTrue(distance == expectedDistance);
    }
}

package com.murthy.ranck.tracks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class TrackPreviousDistanceTest {
    private Track track;

    @Before
    public void setUp() {
        TrackDummyData trackDummyData = new TrackDummyData();
        this.track = trackDummyData.getTrack();
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {0, 0.}, {9, 1.}, {5, 1.}
        });
    }

    @Parameterized.Parameter
    public int index;

    @Parameterized.Parameter(1)
    public double expectedDistance;

    @Test
    public void distanceToPreviousCurveTest() {
        double distance = track.distanceToPreviousCurve(index);
        assertTrue(distance == expectedDistance);
    }

}

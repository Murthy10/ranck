package com.murthy.ranck.utils;


import com.murthy.ranck.utils.LocationConverter;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class LocationConverterTest {
    private LocationConverter locationConverter;

    @Before
    public void setUp() {
        locationConverter = new LocationConverter();
    }


    @Test
    public void llaToEcefTest() {
        double[] expectedEcef = new double[]{4283425.77, 4612587.91, 1027846.12};
        double[] ecef = locationConverter.llaToEcef(new double[]{9.33518959, 47.11903919, 522.0});
        assertArrayEquals(expectedEcef, ecef, 0.1);
    }

    @Test
    public void ecefToLlaTest() {
        double[] expectedLla = new double[]{9.33518959, 47.11903919, 522.0};
        double[] lla = locationConverter.ecefToLla(new double[]{4283425.77, 4612587.91, 1027846.12});
        assertArrayEquals(expectedLla, lla, 0.1);
    }

    @Test
    public void llaToWebMercatorTest() {
        double[] expectedWebMercator = new double[]{1039170.45, 5961522.03};
        double[] webMercator = locationConverter.llaToWebMercator(new double[]{9.335027, 47.119015});
        assertArrayEquals(expectedWebMercator, webMercator, 0.1);
    }


    @Test
    public void webMercatorToLla() {
        double[] expectedLla = new double[]{9.335027, 47.119015};
        double[] lla = locationConverter.webMercatorToLla(new double[]{1039170.45, 5961522.03});
        assertArrayEquals(expectedLla, lla, 0.1);
    }

}

package com.murthy.ranck.utils;

import com.murthy.ranck.tracks.Track;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class LocationEstimatorTest {
    private LocationEstimator locationEstimator;
    private Track track;

    private Coordinate[] dummyCoordinates() {
        Coordinate[] coordinates = new Coordinate[10];
        for (int i = 0; i < 10; i++) {
            Coordinate coordinate = new Coordinate(i, 0.);
            coordinates[i] = coordinate;
        }
        return coordinates;
    }

    @Before
    public void setUp() {
        GeometryFactory geometryFactory = new GeometryFactory();
        LineString lineString = geometryFactory.createLineString(dummyCoordinates());
        track = new Track(lineString, new double[]{}, new double[]{});
        locationEstimator = new LocationEstimator(track, 1000);
    }

    @Test
    public void locationEstimationTest() {
        Coordinate lastKnownLocation = track.getCoordinateByIndex(0);
        locationEstimator.startEstimation(lastKnownLocation);
        Coordinate estimatedLocation = locationEstimator.estimateLocation(3.5);
        Coordinate expectedLocation = track.getCoordinateByIndex(3);
        assertTrue(estimatedLocation.equals2D(expectedLocation));
    }

    @Test
    public void locationEstimationLastTest() {
        Coordinate lastKnownLocation = track.getCoordinateByIndex(9);
        locationEstimator.startEstimation(lastKnownLocation);
        Coordinate estimatedLocation = locationEstimator.estimateLocation(3.5);
        assertTrue(estimatedLocation.equals2D(lastKnownLocation));
    }

    @Test
    public void locationEstimationFirstTest() {
        Coordinate lastKnownLocation = track.getCoordinateByIndex(-1);
        locationEstimator.startEstimation(lastKnownLocation);
        Coordinate estimatedLocation = locationEstimator.estimateLocation(3.5);
        Coordinate expectedLocation = track.getCoordinateByIndex(3);
        assertTrue(estimatedLocation.equals2D(expectedLocation));
    }


}

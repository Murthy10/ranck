package com.murthy.ranck.physics;

import android.content.Context;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SpeedCheckerTest {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {80, 10, 20, 0.8, 5, true}, {80, 15, 20, 0.5, 15, false}, {80, 12, 20, 0.5, 15, false}
        });
    }

    @Parameter
    public double mass;

    @Parameter(1)
    public double speed;

    @Parameter(2)
    public double radius;

    @Parameter(3)
    public double friction;

    @Parameter(4)
    public double slope;

    @Parameter(5)
    public boolean expected;

    @Test
    public void speedCheck() {
        SpeedChecker speedChecker = new SpeedChecker();
        boolean checked = speedChecker.frictionForce(mass, slope, friction) >= speedChecker.centrifugalForce(mass, speed, radius);
        assertEquals(expected, checked);
    }
}

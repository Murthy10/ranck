package com.murthy.ranck.sensors;


import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;


public class KalmanFilter3DTest {

    private KalmanFilter3D kalmanFilter3D;

    @Before
    public void setUp() {
        kalmanFilter3D = new KalmanFilter3D(0.1);
    }


    @Test
    public void rDimensionsCheck() {
        assertTrue(dimensionsCheck(6, 6, kalmanFilter3D.R));
    }

    @Test
    public void xDimensionsCheck() {
        assertTrue(dimensionsCheck(9, 1, kalmanFilter3D.x));
    }

    @Test
    public void hDimensionsCheck() {
        assertTrue(dimensionsCheck(6, 9, kalmanFilter3D.H));
    }

    @Test
    public void pDimensionsCheck() {
        assertTrue(dimensionsCheck(9, 9, kalmanFilter3D.P));
    }

    @Test
    public void aDimensionsCheck() {
        assertTrue(dimensionsCheck(9, 9, kalmanFilter3D.A));
    }

    @Test
    public void qDimensionsCheck() {
        assertTrue(dimensionsCheck(9, 9, kalmanFilter3D.Q));
    }

    private boolean dimensionsCheck(int row, int column, RealMatrix matrix) {
        return matrix.getRowDimension() == row && matrix.getColumnDimension() == column;
    }

    @Test
    public void qTest() {
        RealMatrix Q = kalmanFilter3D.Q;
        rowCheck(Q.getRow(0), new double[]{2.50000000e-11, 2.50000000e-11, 2.50000000e-11, 5.00000000e-10,
                5.00000000e-10, 5.00000000e-10, 5.00000000e-09, 5.00000000e-09, 5.00000000e-09});
    }

    @Test
    public void aTest() {
        RealMatrix A = kalmanFilter3D.A;
        rowCheck(A.getRow(0), new double[]{1.0, 0.0, 0.0, 0.1, 0.0, 0.0, 0.005, 0.0, 0.0,});
    }

    @Test
    public void rTest() {
        RealMatrix R = kalmanFilter3D.R;
        rowCheck(R.getRow(0), new double[]{100.0, 0.0, 0.0, 0.0, 0.0, 0.0});
    }

    private void rowCheck(double[] row, double[] expectedRow) {
        assertArrayEquals(expectedRow, row, 1.0e-12);
    }

    @Test
    public void inverseTest() {
        RealMatrix matrix = new Array2DRowRealMatrix(new double[][]{{2, 5}, {1, 3}});
        RealMatrix inverseMatrix = kalmanFilter3D.getInverse(matrix);
        RealMatrix expectedInverse = new Array2DRowRealMatrix(new double[][]{{3, -5}, {-1, 2}});
        rowCheck(inverseMatrix.getRow(0), expectedInverse.getRow(0));
        rowCheck(inverseMatrix.getRow(1), expectedInverse.getRow(1));
    }

    @Test
    public void processTest() {
        double[] gps = new double[]{0, 0, 0};
        double[] acc = new double[]{0, 0, 0};
        double[] result = kalmanFilter3D.process(acc, gps, true);
        double[] expected = new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        rowCheck(result, expected);
    }

    @Test
    public void process2Test() {
        double[] gps = new double[]{1, 1, 1};
        double[] acc = new double[]{0, 0, 0};
        for(int i = 0; i < 1000; i++){
            kalmanFilter3D.process(acc, gps, true);
        }
        double[] result = kalmanFilter3D.process(acc, gps, true);
        double[] expected = new double[]{1, 1, 1, 0, 0, 0, 0, 0, 0};
        assertArrayEquals(result, expected, 0.1);
    }


}
